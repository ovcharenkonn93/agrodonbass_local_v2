@include('header')
@include('lightboxes/delete_document')

<div class="block-wrapper block-wrapper-main block-wrapper-main-home">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(!empty(Session::get('user')))
                    <div class="col-md-6">
                    <p>Чтобы добавить новый документ на страницу укажите в списке к какому филиалу он относится и нажмите кнопку «Выберите файл».
                        Документ можно добавить сразу в несколько филиалов. Для этого при выборе из списка нужно держать зажатой клавишу Ctrl. Новый филиал можно добавить на странице «<a href="{{ url('/contacts') }}">Контакты</a></li>».
                    </div>
                    <div class="col-md-6 padding-right_0">
                        <select id="office" multiple>
                        @foreach($all_offices as $office)
                            <option value="{{$office->id}}">{{$office->name}}</option>
                        @endforeach
                        </select>
                    <input type="file" id="files"/>
                        <div id="uploading" class="progress progress-striped active" hidden>
                            <div class="progress-bar progress-bar-warning" style="width: 100%;">
                                <span class="sr-only">Загрузка...</span>
                            </div>
                        </div>
                    </div>
                @endif
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <br>

            </div>
        </div>
    </div>
    <div class="container">
        @foreach($offices as $office)
            <div class="row">
                <div class="col-md-12  ad-registration_documents-documents_block">
                    <h4 class="text-center">{{$office->name}}</h4>
                    <div class="col-md-6 ad-contacts-block ad-registration_documents-block text-center">
                        @for($i=0;$i<count($documents = $office->documents_by_current_page)/2;$i++)
                            <table>
                                <tr>
                                    @if(empty(Session::get('user')))
                                        <td class="ad-registration_documents-icon-td">
                                            <a target="_blank" href="{{$documents[$i]->url}}" title="Скачать документ"><i class="fa fa-download yellow-text"></i></a>
                                        </td>
                                        <td class="ad-registration_documents-name-td">
                                            <a target="_blank" href="{{$documents[$i]->url}}"><p>{{$documents[$i]->name}}</p></a>
                                        </td>
                                    @else
                                        <td class="ad-registration_documents-name-td">
                                            <input class="form-control file-name" type="text" value="{{$documents[$i]->name}}" data-original="{{$documents[$i]->name}}" data-id="{{$documents[$i]->id}}" data-office="{{$office->id}}">
                                        </td>
                                    @endif
                                    @if(!empty(Session::get('user')))
                                        <td class="ad-registration_documents-icon-td">
                                            <a class="ddbtn" href="#" data-id="{{$documents[$i]->id}}" data-office="{{$office->id}}" title="Удалить документ"><i class="fa fa-2x fa-remove"></i></a>
                                        </td>
                                    @endif
                                </tr>
                            </table>
                        @endfor
                    </div>
                    <div class="col-md-6 ad-contacts-block ad-registration_documents-block text-center">
                        @for($i=$i;$i<count($documents);$i++)
                            <table>
                                <tr>
                                    @if(empty(Session::get('user')))
                                        <td class="ad-registration_documents-icon-td">
                                            <a target="_blank" href="{{$documents[$i]->url}}" title="Скачать документ"><i class="fa fa-download yellow-text"></i></a>
                                        </td>
                                        <td class="ad-registration_documents-name-td">
                                            <a target="_blank" href="{{$documents[$i]->url}}"><p>{{$documents[$i]->name}}</p></a>
                                        </td>
                                    @else
                                        <td class="ad-registration_documents-name-td">
                                            <input class="form-control file-name" type="text" value="{{$documents[$i]->name}}" data-original="{{$documents[$i]->name}}" data-id="{{$documents[$i]->id}}" data-office="{{$office->id}}">
                                        </td>
                                    @endif
                                    @if(!empty(Session::get('user')))
                                        <td class="ad-registration_documents-icon-td"><a class="ddbtn" href="#" data-id="{{$documents[$i]->id}}" data-office="{{$office->id}}" title="Удалить документ"><i class="fa fa-2x fa-remove"></i></a></td>
                                    @endif
                                </tr>
                            </table>
                        @endfor
                    </div>
                </div>
            </div>
        @endforeach
        @if(!empty(Session::get('user')))
            <button class="btn btn-primary" type="button" id="save"><i class="fa fa-floppy-o"/></i> Сохранить изменения документов</button>
        @endif
    </div>
</div>

<script src="{{asset("public/js/documents/add_delete.js")}}"></script>

@include('footer')