@include('header')

<div class="block-wrapper block-wrapper-title ">
	<div class="container">
		<div class="row">

			<div class="block-title">

				<h1><img id="title_image" src="{{ $picture or '' }}" alt=""> {{$title}}</h1>
				@if(!empty(Session::get('user')))
					<label for="hpic">Загрузить другое изображение:</label>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="file" id="hpic" class="btn hpicedit" content="">
				@endif
			</div>

		</div>
	</div>
</div>
@if($no_find_news)
<h2 style="text-align: center; color: red;">Новость не найдена!</h2>
@endif
<div class="block-wrapper block-wrapper-news">
    <div class="container">
		<div class="row">
			@include('blocks.ad-news')
		</div>	
	</div>
</div>
@include('lightboxes.edit_image')
@include('footer')
