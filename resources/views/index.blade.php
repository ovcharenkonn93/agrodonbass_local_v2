@include('header')
@if(empty(Session::get('user')))
	@include('blocks.ad__home-slider')
@else
	<div class="container">
		@include('blocks.block-home-slider-edit')
	</div>
@endif
<div class="container">
	<div class="block-wrapper block-wrapper-main block-wrapper-main-home">
		<div class="row">
			<div class="col-md-12 margin-top-20px">
				@if(empty(Session::get('user')))
					@include('blocks.block-home-offers')
				@else
					@include('blocks.block-home-offers-edit')
				@endif
			</div>
		</div>
    </div>
</div>
<div class="block-wrapper block-wrapper-main-news">
    <div class="container">
		<div class="row">
			@include('blocks.ad-home-news')
		</div>
	</div>
</div>
@include('lightboxes.notification.wrong_type')
@include('footer')
