<div class="ll-menu-panel-left">
	<a class="ll-menu-panel-trigger" href="#"><i class="fa fa-reorder"></i></a>

	<div class="ll-menu-panel-left-list">
		  <div class="list-group panel">
			<a href="#d1" class="list-group-item list-group-item-success" data-toggle="collapse">Услуги <i class="fa fa-angle-down" aria-hidden="true"></i></a>
			  <div class="collapse" id="d1">
				  				  <a href="{{ url('/trucking') }}" class="list-group-item" data-parent="#SubMenu1">Грузоперевозка</a>
				  <a href="{{ url('/cultivation') }}" class="list-group-item" data-parent="#SubMenu1">Обработка земли</a>
			  </div>
			<a href="#d2" class="list-group-item list-group-item-success" data-toggle="collapse">Торговля <i class="fa fa-angle-down" aria-hidden="true"></i></a>
			  <div class="collapse" id="d2">
				  <a href="{{ url('/for_plants') }}" class="list-group-item" data-parent="#SubMenu1">Удобрения и СЗР</a>
				  <a href="{{ url('/agricultural-products') }}" class="list-group-item" data-parent="#SubMenu1">Сельхозпродукция</a>
			  </div>
			<a href="#d3" class="list-group-item list-group-item-success" data-toggle="collapse">Документы <i class="fa fa-angle-down" aria-hidden="true"></i></a>
			  <div class="collapse" id="d3">
				  <a href="{{ url('/registration_documents') }}" class="list-group-item" data-parent="#SubMenu1">Регистрационные документы</a>
				  <a href="{{ url('/typical_forms') }}" class="list-group-item" data-parent="#SubMenu1">Типовые формы</a>
				  <a href="{{ url('/licenses') }}" class="list-group-item" data-parent="#SubMenu1">Лицензии</a>
			  </div>
			<a href="#d4" class="list-group-item list-group-item-success" data-toggle="collapse">О нас <i class="fa fa-angle-down" aria-hidden="true"></i></a>
			<div class="collapse" id="d4">
				<a href="{{ url('/about') }}" class="list-group-item" data-parent="#SubMenu1">Информация</a>
				<a href="{{ url('/vacancies') }}" class="list-group-item" data-parent="#SubMenu1">Вакансии</a>
				<a href="{{ url('/contacts') }}" class="list-group-item" data-parent="#SubMenu1">Контакты</a>
			</div>






		  </div>
		</div>	
		
</div>
<a class="ll-menu-panel-trigger" href="#"><i class="fa fa-reorder"></i></a>

<script type="text/javascript">
$(document).ready(function(){
    $(".ll-menu-panel-trigger").click(function(){
        $(".ll-menu-panel-left").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
});

$(document).click( function(event){
	if( $(event.target).closest('.ll-menu-panel-left').length )
		return;
	$('.ll-menu-panel-left').fadeOut("normal");
	event.stopPropagation();
});

</script>