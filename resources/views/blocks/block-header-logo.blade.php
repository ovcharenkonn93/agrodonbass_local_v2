<div class="block-header-logo">
	<a href="{{ url('/')}}">
		<img class="block-header-logo-desktop" src="{{ url('public/images/logo.png')}}" alt="ГП «АГРО-ДОНБАСС»" title="ГП «АГРО-ДОНБАСС»">
		<img class="block-header-logo-mobile" src="{{ url('public/images/logo-mobile.png')}}" alt="ГП «АГРО-ДОНБАСС»" title="ГП «АГРО-ДОНБАСС»">
	</a>
</div>