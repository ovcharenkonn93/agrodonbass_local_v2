<div class="col-md-12 ad-news">


		@foreach($news as $key=>$new)
			<div class="col-md-12 margin-top-20px  ad-new1">
                <div class="col-md-6">
					<div class="ad-news-img">
						<a href="news/{{$new->id}}"><img src="{{ url($new->title_img_news)}}"></a>
					</div>
                </div>
                <div class="col-md-6">
					<div class="ad-news-content">
						<div class="row">
							<div class="col-md-11">
								<h3 class="">{{$new->name}}</h3>
							</div>
							@if(!empty(Session::get('user')))
								<div class="col-md-1">
									@if($key != 0)
										<a data-href="{{URL::to("news/sort")}}" data-id="{{$new->id}}" class="sort_news" title="Поднять новость вверх">
											<i class="fa fa-2x fa-arrow-up"></i>
										</a>
									@endif()
									<a data-url="{{URL::to("news/delete/".$new->id)}}" class="delete_news" title="Удалить новость">
										<i class="fa fa-2x fa-remove"></i>
									</a>
								</div>
							@endif
						</div>
						<div class="row">
							<div class="col-md-11">
								<p class="gray-text">{{$new->created_at}}</p>
							</div>
						</div>
						<hr>
						<div class="ad-news-content-text">
							<p class="ad-news-content-p">
								{{$new->description}}
							</p>
						</div>
						<a class="ad__button_2 ad-news-read_more" href="news/{{$new->id}}">
							@if(!empty(Session::get('user')))
								Редактировать новость
							@else
								Читать дальше
							@endif
						</a>
					</div>
                </div>
            </div>
		@endforeach

	<div class="col-md-12 text-center ad-pagination">
		{!! $news->render() !!}
	</div>		
	
</div>
@include("lightboxes.delete_new")