<div class="ad__footer-contacts">
    <div class="row">			 
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="ad__footer-contacts-adr">
                <div class="row">
                    <div class="col-sm-offset-4 col-md-offset-3">
                        <h3 class="ad__footer-contacts-adr-title">ГП «АГРО-ДОНБАСС»</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-3 col-md-offset-2">
                        <div class="col-xs-1 col-sm-2 col-md-2">
                            <i class="fa fa-2x fa-home"></i>
                        </div>
                        <div class="col-xs-10 col-sm-7 col-md-8">
                            <a id="init_map" title="Показать на карте">{{$footer_contact["address"]}}</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-3 col-md-offset-2">
                        <div class="col-xs-1 col-sm-2 col-md-2">
                            <i class="fa fa-2x fa-phone"></i>
                        </div>
                        <div class="col-xs-10 col-sm-7 col-md-8">
                            <p>{{$footer_contact["telephone"][0]}}<br>
                                {{$footer_contact["telephone"][1]}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-3 col-md-offset-2">
                        <div class="col-xs-1 col-sm-2 col-md-2">
                            <i class="fa fa-2x fa-envelope"></i>
                        </div>
                        <div class="col-xs-10 col-sm-7 col-md-8">
                            <p><a href="mailto:{{$footer_contact["email"]}}">{{$footer_contact["email"]}}</a></p>
                        </div>
                    </div>
                </div>
                    </div>
            </div>
            <div class="col-md-6">
                <div class="ad__footer-contacts-list">
                <div class="row">
                    <div class="col-sm-offset-4 col-md-offset-3 margin-top-10px">
                        <ol class="ad__footer-contacts-list-of-services">

                            <li><a href="{{ url('/trucking') }}"><i class="fa fa-angle-right"></i>  Грузоперевозки</a></li>
                            <li><a href="{{ url('/cultivation') }}"><i class="fa fa-angle-right"></i>  Обработка земли</a></li>
                            <li><a href="{{ url('/for_plants') }}"><i class="fa fa-angle-right"></i>  Удобрения и средства защиты растений</a></li>
                            <li><a href="{{ url('/agricultural-products') }}"><i class="fa fa-angle-right"></i>  Сельхозпродукция</a></li>
                        </ol>
                    </div>
                </div>
                </div>
            </div>
        <div class="">	
            <div class="ad-footer-login" id="icon-foot">

                      @if(empty(Session::get('user')))
                        <a href="{{URL::to('login')}}" title="Войти в режим редактирования"><i class="fa fa-2x fa-pencil-square-o "></i> </a>
                      @else
                        <a id="feedback-link" title="Отправить сообщение разработчикам" data-toggle="modal" data-target="#feedback-modal"> <i class="fa fa-2x fa-comments-o"></i> </a>
                        <a href="{{URL::to('logout')}}" title="Выйти из режима редактирования">  <i class="fa fa-2x fa-sign-out"></i> </a>
                      @endif

                      {{--<a href="#">  <i class="fa fa-2x fa-paper-plane-o"></i> </a>--}}
                      {{--<a href="#">  <i class="fa fa-2x fa-google-plus"></i> </a>--}}
                      {{--<a href="#">  <i class="fa fa-2x fa-vk"></i> </a>--}}
                      {{--<a href="#">  <i class="fa fa-2x fa-instagram"></i> </a>--}}
             </div>	
		</div>			
        </div>			
    </div>
</div>
