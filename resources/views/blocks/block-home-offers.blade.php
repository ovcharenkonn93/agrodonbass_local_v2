<div class="block-home-offers col-md-12 text-center">
    <h1 class="yellow-text">Наши услуги</h1>
    <div class="block-home-offer text-center">
        <div class="block-home-offers__element">
            <div class="block-home-offers__image">
                @include('blocks.block-carousel-plant-protection')
            </div>
            <div class="block-home-offers__image-title">
                <h3><a href="{{ url('/traiding') }}">ТОРГОВЛЯ</a></h3>
            </div>
        </div>
    </div>

    <div class="block-home-offer text-center">
        <div class="block-home-offers__element">
            <div class="block-home-offers__image">
                @include('blocks.block-carousel-trucking')
            </div>
            <div class="block-home-offers__image-title">
                <h3><a href="{{ url('/trucking') }}">ГРУЗОПЕРЕВОЗКА</a></h3>
            </div>
        </div>
    </div>

    <div class="block-home-offer text-center">
        <div class="block-home-offers__element">
            <div class="block-home-offers__image">
                @include('blocks.block-carousel-cultivation')
            </div>
            <div class="block-home-offers__image-title">
                <h3><a href="{{ url('/cultivation') }}">ОБРАБОТКА ЗЕМЛИ</a></h3>
            </div>
        </div>
    </div>
</div>