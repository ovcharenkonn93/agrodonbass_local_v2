<div class="ad__header-menu">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse text-center" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <li>Услуги
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('/trucking') }}">Грузоперевозки</a></li>
                        <li><a href="{{ url('/cultivation') }}">Обработка земли</a></li>
                    </ul>
                </li>
                <li>Торговля
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('/for_plants') }}">Удобрения и средства защиты растений</a></li>
                        <li><a href="{{ url('/agricultural-products') }}">Сельхозпродукция</a></li>
                    </ul>
                </li>
                <li>Документы
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('/registration_documents') }}">Регистрационные документы</a></li>
                        <li><a href="{{ url('/typical_forms') }}">Типовые формы</a></li>
                        <li><a href="{{ url('/licenses') }}">Лицензии</a></li>
                    </ul>
                </li>
                <li> О нас
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('/about') }}">Информация</a></li>
                        <li><a href="{{ url('/vacancies') }}">Вакансии</a></li>
                        <li><a href="{{ url('/contacts') }}">Контакты</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</div>

<script>
jQuery('ul.nav > li').hover(function() {

    jQuery(this).find('.dropdown-menu').stop(true, true).delay(70).fadeIn();

}, function() {

    jQuery(this).find('.dropdown-menu').stop(true, true).delay(70).fadeOut();

})
</script>