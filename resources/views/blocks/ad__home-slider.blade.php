<div class="ad-home-visual">
   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Маркеры слайдов -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>

      <!-- Содержимое слайдов -->
      <div class="carousel-inner">
          @foreach($list_img_main_slider as $key => $row)
              <div class="item @if($key==0) active @endif">
                  <img src="{{ url($row->title_img)}}" alt="...">
                  <div class="ad__home-slider-caption">
                      <h1>АГРО-ДОНБАСС</h1>
                      <h3>Государственное предприятие</h3>
                  </div>
              </div>
          @endforeach
      </div>
    </div>
</div>
