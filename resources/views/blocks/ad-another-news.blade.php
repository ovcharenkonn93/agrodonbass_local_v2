<div class="ad-another-news">
	<a href="{{'/news'}}"><h2 class="text-center">Другие новости</h2></a>
		@foreach($another_news as $another_new)
				<div class="col-md-4 ad-another-news__item">
					<div class="item-image">
						<a href="{{$another_new->id}}">
							<img src="{{ url($another_new->title_img_news)}}" alt="">
						</a>
					</div>
					<div class="ad-another-news-item_bottom_text">
						<a href="{{$another_new->id}}">{{$another_new->name}}</a>
					</div>
				</div>
		@endforeach	
</div>