{{ ($office_id = 1) ? '' : '' }}

@if(!empty(Session::get('user')))
	<form method="post" name="edit_contacts" id="edit_contact">
		<input type="hidden" name="_token" form="edit_contact" value="{{ csrf_token() }}">
	@foreach($offices as $key => $office)

		<div @if ($office->id % 2) @else class="gray-bg" @endif>

		    <div class="container">
                <div class="row">
			        <div class="col-md-12">
                        <div class="col-md-6 ad-contacts-block">
                        {{--<h4><i class="ad-contacts-marker">{{$office->id}}</i></h4>--}}
                        <input type="hidden" form="edit_contact" value="{{$office->id}}" name="office[][id]">
                        <div class="row">
                            <div class="col-md-1"><i class="ad-contacts-marker">{{$key + 1}}</i></div>
                            <div class="col-md-10"><input form="edit_contact" type="text" class="form-control" value="{{$office->name}}" name="office[][offices_name]" required></div>
                        </div>

                        <table>
                            <tr>
                                <td><i class="fa fa-building yellow-text"></i></td>
                                {{--<td> <p>Юридический адрес:</p> <p>{{$office->legal_addr}}</p></td>--}}
                                <td> <p>Юридический адрес:</p> <p>
                                        <input type="text" form="edit_contact" class="form-control" value="{{$office->legal_addr}}" name="office[][legal_addr]" style="width:245%" required>
                                    </p></td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td><i class="fa fa-building yellow-text"></i></td>
                                {{--<td> <p>Фактический адрес:</p> <p>{{$office->actual_addr}}</p></td>--}}
                                <td> <p>Фактический адрес:</p> <p><input form="edit_contact" type="text" class="form-control" value="{{$office->actual_addr}}" name="office[][actual_addr]" style="width:245%" required></p></td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td><i class="fa fa-building yellow-text"></i></td>
                                {{--<td> <p>Фактический адрес:</p> <p>{{$office->actual_addr}}</p></td>--}}
                                <td> <p>Email:</p> <p><input form="edit_contact" type="text" class="form-control" value="{{$office->email}}" name="office[][email]" style="width:245%" required></p></td>
                            </tr>
                        </table>
                        </div>
                        <div class="col-md-6 ad-contacts-block">
                            <h4>С нами можно связаться по телефонам:</h4>
                            <table id="office{{$office->id}}" class="office_contact">
                                <tbody>
                                @foreach($contacts as $contact)
                                    @if ($contact->office === $office->id)
                                        <tr>
                                            <input type="hidden" value="{{$contact->id}}" form="edit_contact" name="contact[][id]">
                                            <td><i class="fa fa-phone  ad-contacts-phone_icon"></i></td>
                                            {{--<td> <p>{{$contact->contact_type}} :</p> <p>{{$contact->phone_number}} – {{$contact->position}} {{$contact->name}}</p></td>--}}
                                            <td> <p><input form="edit_contact" type="text" class="form-control" value="{{$contact->contact_type}}" name="contact[][contact_type]" required></p>
                                                <div class="row">
                                                    <div class="col-md-5"><input form="edit_contact" type="text" class="form-control" value="{{$contact->phone_number}}" name="contact[][phone_number]" required></div>
                                                    <div class="col-md-1 ad-contacts-block__dash-labels"> — </div>
                                                    <div class="col-md-5">
                                                        <input form="edit_contact" type="text" class="form-control" value="{{$contact->position}}" name="contact[][position]" required>
                                                        <input form="edit_contact" type="text" class="form-control" value="{{$contact->name}}" name="contact[][contact_name]" required>
                                                        <input form="edit_contact" type="text" class="form-control" value="{{$contact->email}}" name="contact[][email]" required>
                                                    </div>
                                                    <div class="col-md-1"><i class="fa fa-minus del_contact" style="cursor: pointer"></i></div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
			        </div>
			    </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-3">
                        <a  data-url="{{URL::to("contacts/delete_office/".$office->id)}}" class="delete_office btn btn-block btn-primary ad-news-link">
                            <i class="fa fa-remove"></i>&nbsp;Удалить офис
                        </a>
                    </div>

                    <div class="col-md-3">
                        <button class="new_contact_office btn btn-block btn-primary" data-id="{{$office->id}}">
                            <i class="fa fa-plus"></i>&nbsp;Добавить новый контакт
                        </button>
                    </div>

                    <div class="col-md-3">
                        <button class="edit_contact_office btn btn-block btn-primary" type="button" data-id="{{$office->id}}">
                            <i class="fa fa-edit"></i>&nbsp;Сохранить изменения
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </form>
		{{ ($office_id = $office_id + 1) ? '' : '' }}
	@endforeach

        <form method="post" name="add_office" id="add_office">
            <div class="container">
                <div class="row"><h3 class="text-center">Добавить новый офис</h3></div>
                <div class="row" style="border: #FFC100 solid 1px">
                    <div class="col-md-12">
                        <div class="col-md-6 ad-contacts-block">
                            <div class="row">
                                <div class="col-md-offset-1 col-md-2"><p>Филиал:</p></div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10"><input form="add_office" type="text" class="form-control" name="new_office[offices_name]" required></div>
                            </div>
                            <table>
                                <tr>
                                    <td><i class="fa fa-building yellow-text"></i></td>
                                    <td> <p>Юридический адрес:</p> <p>
                                            <input form="add_office" type="text" class="form-control" name="new_office[legal_addr]" style="width:245%" required>
                                        </p></td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td><i class="fa fa-building yellow-text"></i></td>
                                    <td> <p>Фактический адрес:</p> <p><input form="add_office" type="text" class="form-control" name="new_office[actual_addr]" style="width:245%" required></p></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6 ad-contacts-block">
                            <table id="list_new_contact">
                                <tr>
                                    <td><i class="fa fa-phone  ad-contacts-phone_icon"></i></td>
                                    <td>
                                        <div class="row">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <input form="add_office" type="text" class="form-control" name="new_contact[][contact_type]" placeholder="Тип контакта..." required>
                                                </div>
                                            </div>
                                            <div class="row margin-top-10px">
                                                <div class="col-md-5"><input form="add_office" type="text" class="form-control" name="new_contact[][phone_number]" placeholder="Телефон..." required></div>
                                                <div class="col-md-6">
                                                    <input form="add_office" type="text" class="form-control" name="new_contact[][position]" placeholder="Должность..." required>
                                                    <input form="add_office" type="text" class="form-control" name="new_contact[][contact_name]" placeholder="Имя..." required>
                                                    <input form="add_office" type="text" class="form-control" name="new_contact[][email]" placeholder="Почта..." required>
                                                </div>
                                                <div class="col-md-1"><i class="fa fa-plus add_contact" style="cursor: pointer"></i></div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-offset-10 col-md-2" style="margin-bottom: 10px">
                            <input type="submit" form="add_office" class="btn btn-primary" value="Добавить офис" style="float:right;">
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="container">
            <div class="col-md-6 col-md-offset-3 ad-contacts-schedule">
                <h4 class="text-center">ГРАФИК РАБОТЫ</h4>

                <div class="col-md-6 text-center">
                    @for($i = 0; $i < count($schedule)-4; $i++)
                        <input type="hidden" form="edit_contact" value="{{$schedule[$i]->id}}" name="schedule[{{$i}}][id]">
                        <div class="row margin-top-10px">
                            <div class="col-md-2"><i class="ad-contacts-day_marker">{{$schedule[$i]->weekday}}</i></div>
                            <div class="col-md-4"><input form="edit_contact" type="text" class="form-control" value="{{$schedule[$i]->start}}" name="schedule[{{$i}}][start]" required></div>
                            <div class="col-md-1 ad-contacts-block__dash-labels"> — </div>
                            <div class="col-md-4"><input form="edit_contact" type="text" class="form-control" value="{{$schedule[$i]->end}}" name="schedule[{{$i}}][end]" required></div>
                        </div>
                    @endfor
                </div>

                <div class="col-md-6 text-center ">
                    @for($i = count($schedule)-4; $i < count($schedule) - 2; $i++)
                        <input type="hidden" form="edit_contact" value="{{$schedule[$i]->id}}" name="schedule[{{$i}}][id]">
                        {{--@if($schedule[$i]->day_off)--}}
                            {{--<div class="row margin-top-10px">--}}
                                {{--<div class="col-md-2"><i class="ad-contacts-day_marker">{{$schedule[$i]->weekday}}</i></div>--}}
                                {{--<div class="col-md-4">Выходной</div>--}}
                                {{--<div class="col-md-1"><input type="checkbox" class="form-control" name="schedule[{{$i}}][day_off]" checked></div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-offset-1 col-md-11">(консультация по телефонам </div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-1">с</div>--}}
                                {{--<div class="col-md-4"><input form="edit_contact" type="text" class="form-control" value="{{$schedule[$i]->start}}" name="schedule[{{$i}}][start]" required></div>--}}
                                {{--<div class="col-md-1">по</div>--}}
                                {{--<div class="col-md-4"><input form="edit_contact" type="text" class="form-control" value="{{$schedule[$i]->end}}" name="schedule[{{$i}}][end]" required></div>--}}
                                {{--<div class="col-md-1">)</div>--}}
                            {{--</div>--}}
                        {{--@else--}}
                            <div class="row margin-top-10px">
                                <div class="col-md-2"><i class="ad-contacts-day_marker">{{$schedule[$i]->weekday}}</i></div>
                                <div class="col-md-4"><input form="edit_contact" type="text" class="form-control" value="{{$schedule[$i]->start}}" name="schedule[{{$i}}][start]" required></div>
                                <div class="col-md-1 ad-contacts-block__dash-labels"> — </div>
                                <div class="col-md-4"><input form="edit_contact" type="text" class="form-control" value="{{$schedule[$i]->end}}" name="schedule[{{$i}}][end]" required></div>
                            </div>
                        {{--@endif--}}
                    @endfor
                        <input type="hidden" form="edit_contact" value="{{$schedule[count($schedule) - 2]->id}}" name="schedule[{{count($schedule) - 2}}][id]">
                        <div class="row margin-top-10px">
                            <div class="col-md-4">
                                <p><i class="ad-contacts-day_marker">{{$schedule[count($schedule) - 2]->weekday}}</i><i class="ad-contacts-day_marker">{{$schedule[count($schedule) - 1]->weekday}}</i></p>
                            </div>
                            <div class="col-md-4 margin-top-10px">
                                <p>Выходной</p>
                            </div>
                        </div>
                        <div class="row">
                            время консультации по телефонам:
                        </div>
                        <div class="row">
                            <div class="col-md-1 ad-contacts-block__time-labels">с</div>
                            <div class="col-md-4"><input form="edit_contact" type="text" class="form-control" value="{{$schedule[count($schedule) - 2]->start}}" name="schedule[{{count($schedule) - 2}}][start]" required></div>
                            <div class="col-md-1 ad-contacts-block__time-labels">по</div>
                            <div class="col-md-4"><input form="edit_contact" type="text" class="form-control" value="{{$schedule[count($schedule) - 2]->end}}" name="schedule[{{count($schedule) - 2}}][end]" required></div>
                        </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-offset-10 col-md-2  margin-top-20px">
                    <input type="submit" form="edit_contact" class="btn btn-primary" value="Сохранить изменения">
                </div>
            </div>
        </div>

@else
	@foreach($offices as $key => $office)

        <div @if ($office->id % 2) @else class="gray-bg" @endif>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6 ad-contacts-block">
                                <h4><i class="ad-contacts-marker">{{$key + 1}}</i>	{{$office->name}}</h4>
                                <table>
                                    <tr>
                                        <td><i class="fa fa-building yellow-text"></i></td>
                                        <td> <p>Юридический адрес:</p> <p>{{$office->legal_addr}}</p></td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td><i class="fa fa-building yellow-text"></i></td>
                                        <td> <p>Фактический адрес:</p> <p>{{$office->actual_addr}}</p></td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td><i class="fa fa-building yellow-text"></i></td>
                                        <td> <p>Email:</p> <p><a href="mailto:{{$office->email}}">{{$office->email}}</a></p></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6 ad-contacts-block">
                                <h4>С нами можно связаться по телефонам:</h4>
                                <table>
                                    @foreach($contacts as $contact)
                                        @if ($contact->office === $office_id)
                                            <tr>
                                                <td><i class="fa fa-phone  ad-contacts-phone_icon"></i></td>
                                                <td>
                                                    <p>{{$contact->contact_type}} :</p> <p>{{$contact->phone_number}} – {{$contact->position}} {{$contact->name}}</p>
                                                    <p><a href="mailto:{{$contact->email}}">{{$contact->email}}</a></p>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>

	{{ ($office_id = $office_id + 1) ? '' : '' }}

	@endforeach
            <div class="container">
                <div class="col-md-6 col-md-offset-3 ad-contacts-schedule">
                    <h4 class="text-center">ГРАФИК РАБОТЫ</h4>

                    <div class="col-md-6 text-center">
                        @for($i = 0; $i < count($schedule)-4; $i++)
                            <p><i class="ad-contacts-day_marker">{{$schedule[$i]->weekday}}</i>	{{$schedule[$i]->start}} — {{$schedule[$i]->end}}	</p>
                        @endfor
                    </div>

                    <div class="col-md-6 text-center ">
                        @for($i = count($schedule)-4; $i < count($schedule) - 2; $i++)
                            <p><i class="ad-contacts-day_marker">{{$schedule[$i]->weekday}}</i>
                                {{--@if($schedule[$i]->day_off) Выходной <br>--}}
                                    {{--<span class="ad-contacts-day_marker-sunday">(консультация по телефонам</span><br><span class="ad-contacts-day_marker-sunday">с {{$schedule[$i]->start}} по {{$schedule[$i]->end}})</span>--}}
                                {{--@else 	--}}
                                {{$schedule[$i]->start}} — {{$schedule[$i]->end}}</p>
                        @endfor
                        <p><i class="ad-contacts-day_marker">{{$schedule[count($schedule) - 2]->weekday}}</i><i class="ad-contacts-day_marker">{{$schedule[count($schedule) - 1]->weekday}}</i>	Выходной<br>
                        <span class="ad-contacts-day_marker-sunday">(консультация по телефонам</span><br><span class="ad-contacts-day_marker-sunday">с {{$schedule[count($schedule) - 2]->start}} по {{$schedule[count($schedule) - 2]->end}})</span></p>
                    </div>
                </div>
            </div>
@endif

</div>
<script src="{{ url('public/js/contacts/contacts.js') }}"></script>
<script src="{{ url('public/plugins/jquery-serialize-object/jquery.serialize-object.js') }}"></script>
<script src="{{asset("public/plugins/input-mask/jquery.inputmask.bundle.js")}}"></script>
@include('lightboxes.delete_office')

