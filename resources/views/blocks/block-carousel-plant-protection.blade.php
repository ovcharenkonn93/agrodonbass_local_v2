<div class="block-carousel-plant-protection">
    <div id="block-carousel-plant-protection" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($list_title_img['traiding'] as $key => $row)
                <div class="item @if($key==0) active @endif">
                    <img src="{{ url($row->title_img)}}" alt="" title="">
                </div>
            @endforeach
        </div>
    </div>
</div>

