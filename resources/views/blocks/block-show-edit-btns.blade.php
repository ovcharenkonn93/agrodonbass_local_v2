@if(!empty(Session::get('user')))
    <div class="row">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <table class="ad-services-price_table">
            <thead>
            <tr>
                <td>Для добавления новой кнопки напишите её название:</td>
                <td>Затем выберите документ, на который будет ссылаться кнопка:</td>
                <td>Нажимайте «Добавить» когда выбраны название и документ:</td>
            </tr>
            </thead>
            <tr>
                <td>
                    <input class="form-control" id="btn_name_add" type="text">
                </td>
                <td>
                    <select id="btn_document_add" class="form-control">
                        @foreach($offices as $office)
                            <optgroup label="{{$office->name}}">
                                @foreach($office->documents as $office_document)
                                    {{--<option value="{{$office_document->id}}">{{$office_document->name}}</option>--}}
                                    <option value="{{$office_document->id}}">{{$office_document->name}}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                    <p class="add-buttons-description">Новый документ в этот список можно добавить на страницах «<a href="{{ url('/registration_documents') }}">Регистрационные документы</a>», «<a href="{{ url('/typical_forms') }}">Типовые формы</a>» и «<a href="{{ url('/licenses') }}">Лицензии</a>».

                </td>
                <td>
                    <button style="float: none" class="btn btn-primary btn_add" data-page="{{$id_page}}"><i class="fa fa-plus"></i>&nbsp;Добавить новую кнопку</button>
                </td>
            </tr>
        </table>
        @if(count($documents)>0)
            <p>Ниже список кнопок, которые уже есть на странице. Вы можете изменить надпись на кнопке и документ на который она ссылается.
            <table class="ad-services-price_table ad-buttons-edit">
                <thead>
                <tr>
                    <td>Надпись на кнопке</td>
                    <td>Документ, на который ссылается кнопка</td>
                    <td>Действие</td>
                </tr>
                </thead>
                @foreach($documents as $document)
                    <tr>
                        <td>
                            <input class="form-control btn_name" type="text" data-pivid="{{$document->pivot->id}}" data-origin="{{$document->pivot->name}}" value="{{$document->pivot->name}}">
                        </td>
                        <td>
                            <select class="form-control btn_document" data-orig="{{$document->id}}">
                                @foreach($offices as $office)
                                    <optgroup label="{{$office->name}}">
                                        @foreach($office->documents as $office_document)
                                            {{--<option value="{{$office_document->id}}">{{$office_document->name}}</option>--}}
                                            @if($document->id==$office_document->id)
                                                <option selected value="{{$office_document->id}}">{{$office_document->name}}</option>
                                            @else
                                                <option value="{{$office_document->id}}">{{$office_document->name}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <a type="button" class="ddbtn" data-pivid="{{$document->pivot->id}}" data-page="{{$id_page}}"><i class="fa fa-2x fa-remove"></i></a>
                        </td>
                    </tr>{{--<a href="{{  url($document->url) }}" class="ad__button_2 ad__button_2-2-strings">{{$document->pivot->name}}</a>--}}
                    {{--<br />--}}
                @endforeach
                </table>
            <button class="btn btn-primary btn_edit" data-page="{{$id_page}}"><i class="fa fa-floppy-o"></i>&nbsp;Сохранить изменения кнопок</button>
        @endif
    </div>
@else
    <div class="row">
        <div id="trucking_contacts" class="col-md-offset-1 col-md-11 ad-services-contacts-button">
            @foreach($documents as $document)
                <a href="{{  url($document->url) }}" class="ad__button_2 ad__button_2-2-strings">{{$document->pivot->name}}</a>
            @endforeach
        </div>
    </div>

@endif