<div class="block-home-slider-edit col-md-12 text-center">
    <div class="row center-block">
        <h3>Изображения для главного слайдера:</h3>
            @foreach($list_img_main_slider as $row)
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img src="{{ url($row->title_img)}}" alt="" title="">
                    <button data-id="{{$row->id}}" data-img="{{$row->title_img}}" data-type="main" class="btn btn-default title_img_modal">Изменить</button>
                </div>
            @endforeach
    </div>
</div>