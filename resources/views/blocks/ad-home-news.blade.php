<div class="col-md-12 margin-top-20px ad-home-news">
    <h1 class="text-center">Новости</h1>

    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="last-new"> 
	        <div class="ad-home-new-img">
                <img src="{{ url($news['last_new']->title_img_news)}}">
			</div>
            <div class="ad-home-new-text">
                <a href="news/{{$news['last_new']->id}}"><h3 class="text-center">{{$news['last_new']->name}}</h3></a>
                <p class="gray-text">{{$news['last_new']->created_at}}</p>
                <p> {{$news['last_new']->description}} </p>
            </div>
		</div>	
    </div>
	
    <div class="col-lg-6 col-md-6 col-sm-6 ad-news-border-left">
        <div class="last-news"> 
            {{--<h4 class="text-center">Последние новости</h4>--}}
            <div class="ad-last-news-content">
				@foreach($news['news'] as $new)
                    <div class="row">
                        <div class="ad-last-news-content-row margin-top-10px">
                            <div class="ad-last-news-content-img">
                                <a href="news/{{$new->id}}"><img src="{{ url($new->title_img_news)}}"></a>
                            </div>
                            <div class="ad-last-news-content-text">
                                <p class="gray-text">{{$new->created_at}}</p>
                                <a href="news/{{$new->id}}"><h5 class="">{{$new->name}}</h5></a>
                                <p class="ad-last-news-content-p">
                                    {{$new->description}}
                                </p>
                            </div>
                        </div>
                    </div>
				@endforeach			
            </div>
 		</div>	   
	</div>
	<a class="ad-news-link" href="{{ url('/news') }}">Другие новости</a>
    @if(!empty(Session::get('user')))
	    <a class="ad-news-link" href="{{ url('/add_news') }}">Добавить новость</a>
    @endif
</div>