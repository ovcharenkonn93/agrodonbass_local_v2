<div class="block-carousel-trucking">
    <div id="block-carousel-trucking" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($list_title_img['trucking'] as $key => $row)
                <div class="item @if($key==0) active @endif">
                    <img src="{{ url($row->title_img)}}" alt="" title="">
                </div>
            @endforeach
        </div>
    </div>
</div>