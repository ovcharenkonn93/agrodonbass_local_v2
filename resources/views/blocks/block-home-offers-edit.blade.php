<div class="block-home-offers-edit col-md-12 text-center">

    <div class="row center-block">
        <h3>Изображения для слайдера «ТОРГОВЛЯ»:</h3>
        @foreach($list_title_img['traiding'] as $row)
            <div class="col-md-2 col-sm-2 col-xs-2">
                <img src="{{ url($row->title_img)}}" alt="" title="">
                <button data-id="{{$row->id}}" data-img="{{$row->title_img}}" data-type="visualise" class="btn btn-default title_img_modal">Изменить</button>
            </div>
        @endforeach
    </div>

    <div class="row center-block">
        <h3>Изображения для слайдера «ГРУЗОПЕРЕВОЗКА»:</h3>
        @foreach($list_title_img['trucking'] as $row)
            <div class="col-md-2 col-sm-2 col-xs-2">
                <img src="{{ url($row->title_img)}}" alt="" title="">
                <button data-id="{{$row->id}}" data-type="visualise" class="btn btn-default title_img_modal">Изменить</button>

            </div>
        @endforeach
    </div>

    <div class="row center-block">
        <h3>Изображения для слайдера «ОБРАБОТКА ЗЕМЛИ»:</h3>
        @foreach($list_title_img['cultivation'] as $row)
            <div class="col-md-2 col-sm-2 col-xs-2">
                <img src="{{ url($row->title_img)}}" alt="" title="">
                <button data-id="{{$row->id}}" data-type="visualise" class="btn btn-default title_img_modal">Изменить</button>
            </div>
        @endforeach
    </div>
</div>
@include('lightboxes.add_title_img_news')