<div class="block-wrapper block-wrapper-services-contacts">
    <div class="container">
        @if(!empty(Session::get('user')))
            <form method="post" id="edit_contact" name="edit_contact" action="{{URL::to("contact/edit_page")}}">
                <input type="hidden" name="page" value="{{$id_page}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="ad-services-contacts-telephone">
                            <div class="row">
                                <p class="ad-services-contacts-paragraph">Все детали вы можете узнать у менеджера <br> по логистике позвонив на номер:</p>
                            </div>
                            <div class="row">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td><i class="fa fa-phone  ad-contacts-phone_icon"></i>&nbsp;&nbsp;&nbsp;</td>
                                        @if(!empty(Session::get('user')))
                                            <td>
                                                <p id="phone_number">{{$contact[0]->phone_number}}</p>
                                            </td>
                                            <td>
                                                <select name="name" id="name" class="form-control">
                                                    @foreach($contact as $row)
                                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        @else
                                            <td><p>{{$contact[0]->phone_number}} ({{$contact[0]->name}})</p></td>
                                        @endif
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="ad-services-contacts-email">
                            <div class="row">
                                <p class="ad-services-contacts-paragraph">Заявки принимаются на электронную почту:</p>
                            </div>
                            <div class="row">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td><i class="fa fa-envelope ad-contacts-phone_icon" aria-hidden="true"></i></td>
                                        <td><p id="email">{{$contact[0]->email}}</p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!empty(Session::get('user')))
                    <div class="row">
                        <div class="col-md-offset-9 col-md-3 padding-right_0">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Сохранить изменения</button>
                        </div>
                    </div>
            </form>
            @endif
                    <!-- Редактирование кнопок -->
            @include('blocks/block-show-edit-btns')
            {{--<div class="row">--}}
            {{--<div id="trucking_contacts" class="col-md-offset-1 col-md-11 ad-services-contacts-button">--}}
            {{--<a href="{{  url('/public/uploads/documents/Заявка на оказание грузоперевозки (ГП).pdf') }}" class="ad__button_2 ad__button_2-2-strings">Скачать заявку</a>--}}
            {{--<a href="{{  url('/public/uploads/documents/Перевозка - 1 (Заказчик-Грузополучатель) (ГП).pdf') }}" class="ad__button_2">Типовая форма договора<br>(Заказчик-Грузополучатель)</a>--}}
            {{--<a href="{{  url('/public/uploads/documents/Перевозка - 2 (Заказчик - Грузоотправитель) (ГП).pdf') }}" class="ad__button_2">Типовая форма договора<br>(Заказчик-Грузоотправитель)</a>--}}
            {{--</div>--}}
            {{--</div>--}}
    </div>
</div>