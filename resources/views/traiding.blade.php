@include('header')

<div class="block-wrapper block-wrapper-title ">
    <div class="container">
        <div class="row">

            <div class="block-title">
				<h1><img src="{{$picture}}" alt=""> {{$title}}</h1>
				@if(!empty(Session::get('user')))
					<label for="hpic">Загрузить другое изображение:</label>
					<input type="file" id="hpic" class="btn hpicedit">
				@endif
            </div>

        </div>
    </div>
</div>

    <div class="container">
        <div class="row">
			<div class="block-traiding-links col-md-12 text-center">
				<div class="block-traiding-link text-center">
					<div class="block-traiding-link__element">
						<div class="block-traiding-link__image">
							<a href="{{ url('/for_plants') }}"><img src="{{ url('public/uploads/visuals/traiding-02.jpg')}}" alt="" title=""></a>
						</div>
						<div class="block-traiding-link__title">
							<a href="{{ url('/for_plants') }}"><h3>УДОБРЕНИЯ И СЗР</h3></a>
						</div>
					</div>
				</div>
				<div class="block-traiding-link text-center">
					<div class="block-traiding-link__element">
						<div class="block-traiding-link__image">
							<a href="{{ url('/agricultural-products') }}"><img src="{{ url('public/uploads/visuals/traiding-04.jpg')}}" alt="" title=""></a>
						</div>
						<div class="block-traiding-link__title">
							<a href="{{ url('/agricultural-products') }}"><h3>СЕЛЬХОЗПРОДУКЦИЯ</h3></a>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>

@include('lightboxes.add_trucking')
@include('lightboxes.delete_trucking')
@include('lightboxes.edit_trucking')
@include('footer')