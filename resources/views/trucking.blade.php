@include('header')
@include('lightboxes/delete_document')

<div class="block-wrapper block-wrapper-title ">
    <div class="container">
        <div class="row">
            <div class="block-title">

            <h1><img id="title_image" src="{{$picture}}" alt=""> {{$title}}</h1>
            @if(!empty(Session::get('user')))
                    <label for="hpic">Загрузить другое изображение:</label>
                <input type="file" id="hpic" class="btn hpicedit">
            @endif

           </div>

        </div>
    </div>
</div>

<div class="block-wrapper block-wrapper-services-info @if(!empty(Session::get('user'))) block-tinymce-container @endif ">
    <div class="container">
       <div class="row">
           <div id="" class="col-md-12 ad-page-info_blocks">
               <div class="block-home-about @if(!empty(Session::get('user'))) block-tinymce-container @endif">
                         {!! $content !!}
               </div>
           </div>
       </div>
    </div>
</div>

@include('blocks.block-services-contacts')

<div class="block-wrapper block-wrapper-services-price">
    <div class="container">
        <div class="row">
        <h3>Расценки на грузоперевозки:</h3>
        <table class="ad-services-price_table">
            <thead>
                <tr>
                    <td>Вид работ</td>
                    <td>Транспортное средство</td>
                    <td>Тариф</td>
                    @if(!empty(Session::get('user')))
                        <td></td>
                        <td></td>
                    @endif
                </tr>
            </thead>

            <tbody>

                @foreach($trucking as $item)
                    <tr>
                        <td align="left">{{$item->type_work}}</td>
                        <td>{{$item->vehicle}}</td>
                        <td align="left">
                            @foreach($item->tariff as $key => $sub)
                                <p>@if(count($item->tariff) > 1){{$key + 1}})&nbsp;&nbsp;@endif{{$sub->tariff}}</p>
                            @endforeach
                        </td>
                        @if(!empty(Session::get('user')))
                            <td><a data-id="{{$item->id}}" class="edit_trucking" style="cursor: pointer" title="Редактировать запись"><i class="fa fa-2x fa-edit"></i></a></td>
                            <td><a data-href="{{URL::to("trucking/delete/".$item->id)}}" class="delete_trucking" title="Удалить запись" style="cursor: pointer"><i class="fa fa-2x fa-remove"></i></a></td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>

        @if(!empty(Session::get('user')))
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <button class="btn btn-primary" id="add_trucking" title="Добавить запись"><i class="fa fa-plus"></i> Добавить новую запись</button>
                </div>
            </div>
        @endif
    </div>
    </div>
</div>
<script src="{{asset('public/js/documents/btn_delete.js')}}"></script>
<script src="{{asset('public/js/documents/btn_edit.js')}}"></script>
<script src="{{asset('public/js/documents/btn_add.js')}}"></script>
<script src="{{asset('public/js/trucking/trucking.js')}}"></script>
<script>
    $(function(){
        // цикл проходит все теги <p> на странице и убирает пустые
        $('p').each(function(){
            console.log ($(this).text());
            if ($(this).text()=='&nbsp;') {
                $(this).detach(); // скрываем элемент
            }
        });
    });

    $(function(){
       $col= $(".ad-services-info_blocks").children('p').length;
       // console.log ($col);
      //  console.log (screen.width);
        if(($col == 2)||($col==1)){
            $(".ad-services-info_blocks").css('justify-content','center');
            $(".ad-services-info_block").children('p').css('width', '35%');
        }

    });

/*
    $(function(){
        $(window).resize(function() {
            console.log( $(window).width()*1 );
        })
    })
    */

</script>

@include('lightboxes.add_trucking')
@include('lightboxes.delete_trucking')
@include('lightboxes.edit_trucking')
@include('lightboxes.edit_image')
@include('footer')