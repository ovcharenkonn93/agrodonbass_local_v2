@include('header')

<div class="block-wrapper block-wrapper-title ">
	<div class="container">
		<div class="row">
			<div class="block-title">
				<h1><img id="title_image" src="{{$picture}}" alt=""> {{$title}}</h1>
				@if(!empty(Session::get('user')))
					<label for="hpic">Загрузить другое изображение:</label>
					<input type="file" id="hpic" class="btn hpicedit">
				@endif
			</div>
		</div>
	</div>
</div>

<div class="block-wrapper block-wrapper-vacancies-info @if(!empty(Session::get('user'))) block-tinymce-container @endif">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ad-vacancies-info_blocks @if(!empty(Session::get('user'))) block-tinymce-container @endif">
				@include('blocks.block-home-about')
			</div>
		</div>
	</div>
</div>

<div class="block-wrapper block-wrapper-about_us-structure">

	<div class="container">
		<div class="row">
			<div class="col-md-12 ad-about_us-structure">
				<h1>Структура предприятия</h1>
				<img id="structure_image" src="{{ url('/public/uploads/interface/about_us.png') }}" alt="">
				@if(!empty(Session::get('user')))
					{{--<label for="edit_structure">Изменить</label><br />--}}
					<input type="file" id="edit_structure" class="btn btn-primary">
				@endif
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			</div>
		</div>
	</div>

</div>

<script src="{{ url('public/js/about/edit_structure.js') }}"></script>
@include('lightboxes.edit_image')

