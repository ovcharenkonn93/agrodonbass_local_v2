<div class="modal fade edit_image" id="edit_image_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Вы хотите изменить изображение?</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <img width="100%" id="old_image" src="" />
                    </div>
                    <div class="col-md-6 text-center">
                        <img width="100%" id="new_image" src="" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-center">
                        <label for="old_image">Текущее изображение</label>
                    </div>
                    <div class="col-md-6 text-center">
                        <label for="old_image">Новое изображение</label>
                    </div>
                </div>
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <a class="btn btn-primary" id="edit_image">
                    <i class="fa fa-edit"></i>&nbsp;Изменить
                </a>
                <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
//    $('#old_image')
    $('#edit_image').on('click', function(){
        if(im_type=="hpic"){
            var fd = new FormData();
            fd.append("file", $("#hpic")[0].files[0]);
            fd.append("url",window.location.pathname);
            fd.append("_token",$("input[name='_token']").val());
            $.ajax({
                url: 'edit_page_picture',
                type: "post",
                dataType: "json",
                contentType: false,
                processData: false,
                data: fd,
                success: function (data) {
                    switch (data){
                        case 1: alert("Файл слишком большой! Размер файла не должен превышать 5 Мб!");
                            break;
                        case 2: alert("Поддерживаются только файлы изображений JPG, PNG и GIF!");
                            break;
                        default:
                            location.reload(true);
                    };
                },
                error: function(errors)
                {
                    console.log(errors);
                }
            });
        }
        else{
            var fd = new FormData();
            fd.append("file", $('#edit_structure')[0].files[0]);
            fd.append("_token",$("input[name='_token']").val());

            $.ajax({
                url: 'edit_structure',
                type: "post",
                dataType: "json",
                contentType: false,
                processData: false,
                data: fd,
                success: function (data) {
                    switch (data){
                        case 1: alert("Файл слишком большой! Размер файла не должен превышать 5 Мб!");
                            break;
                        case 2: alert("Поддерживаются только файлы изображений JPG, PNG и GIF!");
                            break;
                        default:
                            location.reload(true);
                    };

                },
                error: function(errors)
                {
                    console.log(errors);
                }
            });
        }
    });
</script>