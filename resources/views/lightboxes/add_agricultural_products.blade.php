<div class="modal fade add_agricultural_products">
    <div class="modal-dialog">
        <form action="{{URL::to("agricultural-products/add")}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="title"></h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Название</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Описание</label>
                        </div>
                        <div class="col-md-6">
                            <textarea name="description" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Стоимость</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="cost" class="form-control" required>
                        </div>
                    </div>
                    <input type="hidden" name="action">
                    {{--<div class="row margin-top-10px">--}}
                        {{--<div class="col-md-6">--}}
                            {{--<label>Действие</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                            {{--<select name="action" class="form-control">--}}
                                {{--<option value="0">Продажа</option>--}}
                                {{--<option value="1">Покупка</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary" id="add_for_plants" type="submit">
                        <i class="fa fa-plus"></i>&nbsp;Добавить
                    </button>
                    <button type="button" id="close" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>