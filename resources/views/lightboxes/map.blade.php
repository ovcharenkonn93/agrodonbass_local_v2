<div id="map_modal" class="modal fade add_cultivation">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Расположение офиса</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <div id="map" style="width: 100%; min-height: 350px;">

                </div>
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">

                <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    ymaps.ready(init);
    var map;

    function init(){
        map = new ymaps.Map("map", {
            center: [55.76, 37.64],
            zoom: 7
        });

        $('#init_map').on('click', function(){
            $('#map_modal').modal('show');

            var office = ymaps.geocode($('#init_map').html()).then(
                    function (res) {
                        var firstGeoObject = res.geoObjects.get(0),
                        coords = firstGeoObject.geometry.getCoordinates(),
                        bounds = firstGeoObject.properties.get('boundedBy');
                        map.geoObjects.add(firstGeoObject);
                        map.setBounds(bounds, {checkZoomRange: true});

                    },
                    function(err){
                        console.log(err);
                    }
            );
        });
    }
</script>