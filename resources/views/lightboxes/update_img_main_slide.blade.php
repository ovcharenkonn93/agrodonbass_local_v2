<div class="modal fade edit_main_slide">
    <div class="modal-dialog">
        <form method="post" action="{{URL::to('edit_main_slide')}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4>Изменение изображения для слайдера</h4>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2 col-md-8 col-sm-8 col-xs-8" id="title_img">
                            <img id="title_img" src="#" alt=""/>
                            <input type="file" name="title_img" required/>
                        </div>
                        <input type="hidden" name="x1">
                        <input type="hidden" name="y1">
                        <input type="hidden" name="x2">
                        <input type="hidden" name="y2">
                        <input type="hidden" name="w">
                        <input type="hidden" name="h">
                        <input type="hidden" name="div_w">
                        <input type="hidden" name="div_h">
                    </div>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary update_title_img"  type="submit">
                        <i class="fa fa-floppy-o"></i>&nbsp;Сохранить
                    </button>
                    <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>