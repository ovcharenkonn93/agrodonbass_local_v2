<div class="modal fade add_trucking">
    <div class="modal-dialog">
        <form action="{{URL::to("trucking/add")}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4>Добавление грузоперевозки</h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Вид работ</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="type_work" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Транспортное средство</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="vehicle" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Тариф</label>
                        </div>
                        <div class="col-md-5">
                            <input type="text" name="tariff[]" class="form-control" required>
                        </div>
                        <div class="col-md-1"><i class="fa fa-plus add_tariff" style="cursor: pointer"></i></div>
                    </div>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary" id="add_trucking" type="submit">
                        <i class="fa fa-plus"></i>&nbsp;Добавить
                    </button>
                    <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>