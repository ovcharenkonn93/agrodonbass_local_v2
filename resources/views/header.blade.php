<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="{{$description}}">
    <meta name="keywords" content=" {{$meta_keywords}} "/>
    <title>«АГРО-ДОНБАСС» — {{$title}}</title>
   <!-- <link href="{{ url('public/css/menu.css') }}" rel="stylesheet">  -->
    <link href="{{ url('public/css/reset.css') }}" rel="stylesheet">
    <link href="{{ url('public/css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{ url('public/css/animate.css') }}" rel="stylesheet">
    <link href="{{ url('public/css/agrodonbass.css') }}" rel="stylesheet">
    <link href="{{ url('public/plugins/Jcrop/css/jquery.Jcrop.css') }}" rel="stylesheet">
    <![if !IE]>
    <link href="{{ url('public/css/ie.css') }}" rel="stylesheet">
    <![endif]>


	

    <!-- temporarily css style -->
	<link rel="stylesheet" href="{{ url('css/style.css')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="{{ url('public/js/jquery-3.0.0.min.js') }}"></script>
    <script src="{{ url('public/js/bootstrap.js') }}"></script>
    <script src="{{ url('public/js/slider/highlight.pack.js') }}"></script>
    <script src="{{ url('public/js/slider/tabifier.js') }}"></script>
    <script src="{{ url('public/js/slider/js.js') }}"></script>
    <script src="{{ url('public/js/slider/jPages.js') }}"></script>
	<script src="{{ url('public/js/counter/jquery.knob.min.js') }}"></script>
    <script src="{{ url('public/js/jquery-ui.js')}}"></script>
    <script src="{{ url('public/js/bootstrap-dropdown.js')}}"></script>
    <script src="{{ url('public/js/bootstrap-select.min.js')}}"></script>

    <script src="{{asset("public/plugins/tinymce/tinymce.min.js")}}"></script>
    <script src="{{asset("public/plugins/Jcrop/js/jquery.Jcrop.min.js")}}"></script>
    <script src="{{ url('public/js/jquery.bootstrap-growl.min.js') }}"></script>
    {{--<script src="{{asset("public/plugins/uploadfile/jquery.uploadify.min.js")}}" type="text/javascript"></script>--}}
    <script src="{{asset("public/js/news/news.js")}}"></script>
    <script src="{{asset("public/js/pages/pages.js")}}"></script>
    <script src="{{asset("public/js/image_preload/image_preload.js")}}"></script>

<!-- pagination -->
	<script src="{{ url('public/js/pagination.js')}}"></script>
<!-- pagination -->

<!-- rotate_slider  -->
    <script src="{{ url('public/js/rotate_slider/app.js')}}"></script>
    <script src="{{ url('public/js/rotate_slider/jquery.rotateSlider.js')}}"></script>
<!-- End references for rotate_slider -->

<!-- Yandex Map -->
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>


</head>

<body>

@include('blocks.ad-menu-panel-left')

<div class="block-wrapper-header">
    <div class="container">
        <div class="row">
                    <div id="left_side-of-header_menu" class="col-md-offset-1 col-md-3 text-center">
						@include('blocks.block-header-logo')
                    </div>

                    <div id="right_side-of-header_menu" class=" col-md-8">
						@include('blocks.ad__header-menu')
                    </div>

                    @if(empty(Session::get('user')))
                        {{--мета теги --}}
                    @else
                        <div class="row {{ $MetaVisibility or '' }}">
                            @include('blocks.block-meta-edit')
                        </div>
                    @endif

        </div>
    </div>
</div>