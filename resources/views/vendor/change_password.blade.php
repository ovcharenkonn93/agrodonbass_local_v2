@section('title')
    {{$title=1}}
@stop

@section('description')
    {{$description=1}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords=1}} "/>@stop

@include('header')

<div class="row text-center">
    @include('blocks.change_password')
</div>

@include('footer')