@include('header')
<div class="block-wrapper block-wrapper-title ">
    <div class="container">
        <div class="row">
            <div class="block-title">
                <h1><img id="title_image" src="{{$picture}}" alt=""> {{$title}}</h1>
                @if(!empty(Session::get('user')))
                    <label for="hpic">Загрузить другое изображение:</label>
                    <input type="file" id="hpic" class="btn hpicedit">
                @endif
            </div>
        </div>
    </div>
</div>


<div class="block-wrapper block-wrapper-vacancies-info @if(!empty(Session::get('user'))) block-tinymce-container @endif">
<div class="container">
    <div class="row">
        <div class="col-md-12 ad-vacancies-info_blocks @if(!empty(Session::get('user'))) block-tinymce-container @endif">
            @include('blocks.block-vacancies')
        </div>
    </div>
</div>
</div>

<div class="block-wrapper block-wrapper-vacancies-contacts">
    <div class="container">
        @if(!empty(Session::get('user')))
            <form method="post" id="edit_contact" name="edit_contact" action="{{URL::to("contact/edit_page")}}">
                <input type="hidden" name="page" value="{{$id_page}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @endif
        <div class="row">
            <div class="col-md-12 ad-vacancies-contacts_block">
                <div class="contacts-block">
                    <p>По открытым вакансиям на сайте, Вы можете отправить своё резюме на нашу электронную почту <b>gpagro-donbass@ya.ru</b>, обязательно указав в теме письма и резюме желаемую должность.</p>
                    @if(!empty(Session::get('user')))
                        <div class="row">
                            <div class="col-md-6">
                                <p>Контактный телефон отдела кадров</p>
                            </div>
                            <div class="col-md-3">
                                <p id="phone_number">{{$contact[0]->phone_number}}</p>
                            </div>
                            <div class="col-md-3">
                                <select name="name" id="name" class="form-control">
                                    @foreach($contact as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @else
                        <p>Контактный телефон отдела кадров <b>{{$contact[0]->phone_number}}</b> ({{$contact[0]->name}})</p>
                    @endif
                </div>
            </div>
        </div>
        @if(!empty(Session::get('user')))
                <div class="row">
                    <div class="col-md-offset-9 col-md-2">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;Редактировать</button>
                    </div>
                </div>
            </form>
        @endif
    </div>
</div>
<div class="block-wrapper block-wrapper-vacancies-list">
    <div class="container">
        @foreach($offices as $office)
            <h3>{{$office->name}}</h3>
            <table class="ad-services-price_table">
                <thead>
                    <tr>
                        <td>Вакансия</td>
                        <td>Пол</td>
                        <td>Образование</td>
                        <td>Требования к кандидату</td>
                        <td>Занятость</td>
                        @if(!empty(Session::get('user')))
                            <td></td>
                            <td></td>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($office->vacancies as $item)
                        <tr>
                            <td>{{$item->vacancies}}</td>
                            <td>{{$item->gender}}</td>
                            <td>{{$item->education}}</td>
                            <td>{{$item->experience}}</td>
                            <td>{{$item->employment}}</td>
                            @if(!empty(Session::get('user')))
                                <td><a data-id="{{$item->id}}" class="edit_vacancies" style="cursor: pointer" title="Редактировать вакансию"><i class="fa fa-2x fa-edit"></i></a></td>
                                <td><a data-href="{{URL::to("vacancies/delete/".$item->id)}}" class="delete_vacancies" title="Удалить вакансию" style="cursor: pointer"><i class="fa fa-2x fa-remove"></i></a></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endforeach
    </div>
    @if(!empty(Session::get('user')))
        <div class="row">
            <div class="col-md-offset-9 col-md-2">
                <button class="btn btn-primary add_vacancies"><i class="fa fa-plus"></i>&nbsp;Добавить новую вакансию</button>
            </div>
        </div>
    @endif
</div>
<script src="{{ url('public/js/vacancies/vacancies.js') }}"></script>

@include('lightboxes.add_vacancies')
@include('lightboxes.delete_vacancies')
@include('lightboxes.edit_vacancies')
@include('lightboxes.edit_image')
@include('footer')