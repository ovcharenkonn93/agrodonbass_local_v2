@include('header',['MetaVisibility'=>'hide-meta-tags-edit'])

<div class="block-wrapper">
    <div class="container block-news-wrapper">
		<div class="row">
			@include('blocks.ad-new')			
		</div>
		<div class="row">
			@include('blocks.block-new-social-buttons')				
		</div>		
	</div>	
    <div class="ad-another-news-wrapper">
			<div class="container">
				@include('blocks.ad-another-news')				
			</div>	
	</div>				
</div>

@include('footer')
