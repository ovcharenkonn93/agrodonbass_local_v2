@include('header')
@include('lightboxes/delete_document')
<div class="block-wrapper block-wrapper-title ">
	<div class="container">
		<div class="row">
			<div class="block-title">

				<h1><img id="title_image" src="{{$picture}}" alt=""> {{$title}}</h1>
				@if(!empty(Session::get('user')))
					<label for="hpic">Загрузить другое изображение:</label>
					<input type="file" id="hpic" class="btn hpicedit">
				@endif
			</div>
		</div>
	</div>
</div>

<div class="block-wrapper block-wrapper-services-info @if(!empty(Session::get('user'))) block-tinymce-container @endif">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ad-page-info_blocks">
				<div class="block-home-about @if(!empty(Session::get('user'))) block-tinymce-container @endif">
						{!! $content !!}
				</div>
			</div>
		</div>
	</div>
</div>

<div class="block-wrapper block-wrapper-services-contacts">
	<div class="container">
		@if(!empty(Session::get('user')))
			<form method="post" id="edit_contact" name="edit_contact" action="{{URL::to("contact/edit_page")}}">
				<input type="hidden" name="page" value="{{$id_page}}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
		@endif
		<div class="row">
			<div class="col-md-6">
				<div class="ad-services-contacts-telephone">
				<div class="row">
					<p class="ad-services-contacts-paragraph">Все детали вы можете узнать у менеджера <br> позвонив на номер:</p>
				</div>
				<div class="row">
					<table>
						<tbody>
						<tr>
							<td><i class="fa fa-phone  ad-contacts-phone_icon"></i></td>
							@if(!empty(Session::get('user')))
								<td>
									<p id="phone_number">{{$contact[0]->phone_number}}</p>
								</td>
								<td>
									<select name="name" id="name" class="form-control">
										@foreach($contact as $row)
											<option value="{{$row->id}}">{{$row->name}}</option>
										@endforeach
									</select>
								</td>
							@else
								<td><p>{{$contact[0]->phone_number}} ({{$contact[0]->name}})</p></td>
							@endif
						</tr>
						</tbody>
					</table>
				</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="ad-services-contacts-email">
					<div class="row">
						<p class="ad-services-contacts-paragraph">Заявки принимаются на электронную почту:</p>
					</div>
					<div class="row">
						<table>
							<tbody>
							<tr>
								<td><i class="fa fa-envelope ad-contacts-phone_icon" aria-hidden="true"></i></td>
								<td><p id="email">{{$contact[0]->email}}</p></td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		@if(!empty(Session::get('user')))
			<div class="row">
				<div class="col-md-offset-9 col-md-2">
					<button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;Редактировать</button>
				</div>
			</div>
			</form>
		@endif
		<!-- Редактирование кнопок -->
		@include('blocks/block-show-edit-btns')
		{{--<div class="row">--}}
			{{--<div class="col-md-offset-3 col-md-6 ad-services-contacts-button">--}}
				{{--<a href="{{ url('/public/uploads/documents/Заявка на оказание услуг (МТС).pdf') }}" class="ad__button_2">Скачать заявку </a>--}}
				{{--<a href="{{ url('/public/uploads/documents/Договор оказания услуг (МТС).pdf') }}" class="ad__button_2">Типовая форма договора</a>--}}
			{{--</div>--}}
		{{--</div>--}}
	</div>
</div>

<div class="block-wrapper block-wrapper-services-price">
	<div class="container">
		<h3>Расценки на услуги по обработке земли:</h3>
		<table class="ad-services-price_table">
			<thead>
				<tr>
					<td>Наименование</td>
					<td>Описание</td>
					<td>Стоимость</td>
					@if(!empty(Session::get('user')))
						<td></td>
						<td></td>
					@endif
				</tr>
			</thead>

			<tbody>
			@foreach($cultivation as $item)
				<tr>
					<td>{{$item->plowing}}</td>
					<td>{{$item->aggregates}}</td>
					<td>{{$item->cost_services}}&nbsp;{{$item->units}}</td>
					@if(!empty(Session::get('user')))
						<td><a data-id="{{$item->id}}" class="edit_cultivation" style="cursor: pointer" title="Редактировать запись"><i class="fa fa-2x fa-edit"></i></a></td>
						<td><a data-href="{{URL::to("cultivation/delete/".$item->id)}}" class="delete_cultivation" title="Удалить запись" style="cursor: pointer"><i class="fa fa-2x fa-remove"></i></a></td>
					@endif
				</tr>
			@endforeach
			</tbody>
		</table>
		@if(!empty(Session::get('user')))
			<div class="row">
				<div class="col-md-offset-10 col-md-2">
					<button class="btn btn-primary" id="add_cultivation" title="Добавить запись"><i class="fa fa-plus"></i>&nbsp;Добавить новую запись</button>
				</div>
			</div>
		@endif
	</div>
</div>


{{--@if(!empty(Session::get('user')))--}}
{{--<div class="block-wrapper block-wrapper-main block-wrapper-main-home">--}}
	{{--<div class="container">--}}
		{{--<div class="row">--}}
			{{--<div class="col-md-12">--}}
	               {{--<form id="add_csv" method="post" action="{{URL::to("cultivation_csv")}}" enctype="multipart/form-data">--}}
				                           {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                {{--<input class="input-required" name="file_csv" type="file" required>--}}
                                {{--<button type="submit" class="btn btn-block btn-flat btn-primary ad-news-link">--}}
                                    {{--<i class="fa fa-plus"></i>&nbsp;Добавить--}}
                                {{--</button>--}}
                   {{--</form>--}}
			{{--</div>--}}
		{{--</div>	--}}
	{{--</div>	--}}
{{--</div>--}}
{{--@endif--}}
<script src="{{asset('public/js/documents/btn_delete.js')}}"></script>
<script src="{{asset('public/js/documents/btn_edit.js')}}"></script>
<script src="{{asset('public/js/documents/btn_add.js')}}"></script>
<script src="{{asset("public/plugins/input-mask/jquery.inputmask.bundle.js")}}"></script>
<script src="{{asset('public/js/cultivation/cultivation.js')}}"></script>

<script>
	$(function(){
		$col= $(".ad-services-info_blocks").children('p').length;
		// console.log ($col);
		if(($col == 2)||($col==1)){
			$(".ad-services-info_blocks").css('justify-content','center');
		}

	});

	$(function(){
		// цикл проходит все теги <p> на странице и убирает пустые
		$('p').each(function(){
			console.log ($(this).text())
			if ($(this).text()=='&nbsp;') {
				$(this).detach(); // скрываем элемент
			};
		});
	});
</script>

@include('lightboxes.add_cultivation')
@include('lightboxes.delete_cultivation')
@include('lightboxes.edit_cultivation')
@include('lightboxes.edit_image')
@include('footer')