		<footer id="footer">
			<div class="llg-wrapper llg-wrapper-footer">
				@include('blocks.ad__footer-contacts')
			</div>
			@include('lightboxes.feedback')
			@include('lightboxes.map')
		</footer>
	</body>
</html>