<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visuals extends Model
{
    protected $table = 'visuals';
    public $timestamps = false;

    protected $fillable = ['title_img', 'own'];
}