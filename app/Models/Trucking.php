<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trucking extends Model
{
    protected $table = 'trucking';
    public $timestamps = false;

    protected $fillable = ['type_work','vehicle'];

    public function tariff() {
        return $this->hasMany('App\Models\Tariff_Trucking','id_trucking','id');
    }

}