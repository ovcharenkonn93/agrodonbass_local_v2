<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Main_Slider extends Model
{
    protected $table = 'main_slider';
    public $timestamps = false;

    protected $fillable = ['title_img'];
}