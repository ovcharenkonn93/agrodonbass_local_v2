<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    protected $table = 'documents';
    public $timestamps = false;

    protected $fillable = ['name','url','id_page'];

    public function offices(){
        return $this->BelongsToMany('App\Models\Offices','offices_documents','id_document','id_office');
    }

    public function page(){
        return $this->belongsTo('App\Models\Site_Pages\Site_Pages','id_page','id');
    }

    public function pages(){
        return $this->belongsToMany('App\Models\Site_Pages\Site_Pages','pages_documents','id_document','id_page')->withPivot('name','id');
    }
}