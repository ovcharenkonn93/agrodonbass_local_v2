<?php
namespace App\Helpers;
use DateTime;

class RSSHelper {

    public function __construct(Manager $manager) {
//        $this->managet = $manager;
    }

    public static function print_rss($news)
    {
        $rss="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<rss version=\"2.0\">
		<channel>
			<language>ru</language>
			<title>Новости АгроДонбасса</title>
			<link>".url("")."</link>
			<description>Новостная рассылка государственного предприятия АгроДонбасс</description>
			<ttl>60</ttl>";
		foreach($news as $new){
			$d=new DateTime($new->created_at);
			$rss.="
			<item>
				<title>$new->name</title>
				<link>".url("news/".$new->id)."</link>
				<description>$new->description</description>
				<pubDate>".$d->format("D, d M Y H:i:s O")."</pubDate>
				<enclosure url=\"".url($new->title_img_news)."\" length=\"".filesize($new->title_img_news)."\" type=\"image/jpeg\"/>
				<guid>".url("news/".$new->id)."</guid>
			</item>";
		}
		
		$rss.="
		</channel>
	</rss>";
		return $rss;
    }
}


