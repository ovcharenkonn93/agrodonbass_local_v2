<?php
namespace App\Helpers;

class ImageHelper {

    private static function imagecreate($type, $src) {
        switch ($type) {
            case 'gif':
                return imagecreatefromgif($src);
                break;
            case 'png':
                return imagecreatefrompng($src);
                break;
            case 'jpg' || 'jpeg':
                return imagecreatefromjpeg($src);
                break;
        }
    }

    private static function imageanytype($dest, $filename, $quality) {
        $type = explode('.', $filename)[count(explode('.', $filename))-1];
        switch ($type) {
            case 'gif':
                imagegif($dest, $filename);
                break;
            case 'png':
                imagepng($dest, $filename, $quality/10);
                break;
            case 'jpg' || 'jpeg':
                imagejpeg($dest, $filename, $quality);
                break;
        }
    }

    public static function imagecreatefromanytype($src, $filename, $w, $h, $div_w, $div_h, $x1, $y1, $max_width, $max_height) {
        $img = ImageHelper::imagecreate($src->getClientOriginalExtension(), $src);
        $dest = imagecreatetruecolor($w * (imagesx($img) / $div_w), $h * (imagesy($img) / $div_h));
        imagecopy($dest, $img, 0, 0, $x1 * (imagesx($img) / $div_w), $y1 * (imagesy($img) / $div_h), $w * (imagesx($img) / $div_w), $h * (imagesy($img) / $div_h));
        ImageHelper::imageanytype($dest, $filename , 90);
        if ($w * (imagesx($img) / $div_w) > $max_width && $h * (imagesy($img) / $div_h) > $max_height) {
            $type = explode('.', $filename)[count(explode('.', $filename))-1];
            $img = ImageHelper::imagecreate($type, $filename);
            $dest = imagecreatetruecolor($max_width, $max_height);
            imagecopy($dest, $img, 0, 0, 0, 0, $max_width, $max_height);
            ImageHelper::imageanytype($dest, $filename , 90);
        }
    }
}


