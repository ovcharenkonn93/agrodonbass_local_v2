<?php

namespace App\Http\Controllers\Controller_News;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResizeImage;
use App\Models\Main_Slider;
use App\Models\Models_News\News;
use App\Models\Offices;
use App\Models\Contacts;
use App\Models\Site_Pages\Site_Pages;
use App\Models\Visuals;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Helpers\BackupDatabaseHelper;
use App\Helpers\RSSHelper;
use App\Helpers\ImageHelper;

class Controller_News extends Controller
{


//Схема вызова функции:
//ResizeImage($image_from,$image_to, $максимальная_ширина,$максимальная_высота,$jpg_качество_уменьшенного_изображения)

//Пример:
//Resizeimage("tempimage.jpg","i/news/$newid-mini.jpg",550,500,90);


	public function index() {
		
		//Заголовок и описание страницы
		$page = \DB::table('site_pages')->where('url', '/')->get();		
			if (!$page) {					
				return \View::make('errors.404');					
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->meta_description;
				  $meta_keywords=$p->meta_keywords;

				!empty(Session::get('page')) ? Session::forget('page') : false;
				Session::put('page',URL::current());
			}
			
		//Вывод новостей
        $last_new = News::orderBy('priority', 'desc')->first();
        $news = News::orderBy('priority','desc')->whereNotIn('id',[$last_new->id])->take(3)->get();

        $list = [];

        foreach($news as $new) {
            $list['news'][] = (object) ['id' => $new->id, 'name' => $new->name, 'description' => $new->description,
                       'content' => $new->content, 'created_at' => date('d.m.Y', strtotime($new->created_at)),
					   'title_img_news' => $new->title_img_news];
        }

//        foreach($last_new as $new)
        $list['last_new'] = (object) ['id' => $last_new->id, 'name' => $last_new->name, 'description' => $last_new->description,
            'content' => $last_new->content, 'created_at' => date('d.m.Y', strtotime($last_new->created_at)), 'title_img_news' => $last_new->title_img_news];

		$visuals = Visuals::orderBy("id","asc")->get();

		$list_title_img = [];

		foreach($visuals as $item) {
			switch($item->own) {
				case 'trucking':
					$list_title_img['trucking'][] = $item;
					break;
				case 'traiding':
					$list_title_img['traiding'][] = $item;
					break;
				case 'cultivation':
					$list_title_img['cultivation'][] = $item;
					break;
			}
		}

		$list_img_main_slider = Main_Slider::orderBy('id', 'asc')->get();

		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
        return view('index',['list_img_main_slider' => $list_img_main_slider,'list_title_img' => $list_title_img,'footer_contact'=>$footer_contact,'news' => $list, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords]);
    }

	public function form_add() {

		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
		$path = 'public/uploads/news';
        
		if(!is_dir($path))     
			
			mkdir($path,0777,true); 
			
		return view('add_news',['footer_contact'=>$footer_contact]);
	}  
	
	public function add_news(Request $request) {
//		dd($request->all());
		$news = News::create(['name' => $request->title, 'description' => $request->description, 'content' => $request->content_news,
		'created_at' => date('Y-m-d H:i:s',strtotime($request->date_create)), 'priority' => News::orderBy("priority",'desc')->first()->priority + 1,
		'meta_keywords' => "", 'meta_description' => ""]);
		if( isset($request->title_img) ) {
			$path = 'public/uploads/news';
			if(!is_dir($path))
				mkdir($path,0777,true);
			$request->title_img->move($path, 'news'.$news->id.'.'.$request->title_img->getClientOriginalExtension());
			$news->title_img_news = $path.'/news'.$news->id.'.'.$request->title_img->getClientOriginalExtension();
			$news->save();
			ResizeImage::ResizeImage($path.'/news'.$news->id.'.'.$request->title_img->getClientOriginalExtension(),$path.'/news'.$news->id.'.'.$request->title_img->getClientOriginalExtension());
		}
		return redirect('/');     
	}
	
	public function news_page(Request $request) { //Новости на сранице новостей
		
		//Заголовок и описание страницы
		$page = \DB::table('site_pages')->where('url', '/news')->get();		
			if (!$page) {					
				return \View::make('errors.404');					
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->meta_description;
				  $meta_keywords=$p->meta_keywords;
				  $picture=$p->picture;
				  !empty(Session::get('page')) ? Session::forget('page') : false;
				  Session::put('page',URL::current());
			}
			
		//Вывод новостей
		if($request->no_find_news){
            $no_find_news=1;
        }
        else{
            $no_find_news=0;
        }
        $news = News::orderBy('priority','desc')->paginate(3);
		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
        return view('news',['no_find_news'=>$no_find_news, 'news' => $news, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'picture'=>$picture, 'footer_contact'=>$footer_contact]);
    }
	
	public function new_page($new_id) { //Одна новость

		$page = \DB::table('news')->where('id', $new_id)->get();

			if (count($page)==0){
				//return \View::make('errors.404');
                //return Controller_News::news_page();
                return redirect('/news?no_find_news=1');
			}

			foreach ($page as $p) {
				  $id=$p->id;
				  $title=$p->name;
				  $description=$p->description;
				  $meta_keywords=$p->meta_keywords;
				  $content =$p->content ;
				  $created_at=date('d.m.Y', strtotime($p->created_at));
				  $updated_at=$p->updated_at;
				  $title_img_new=$p->title_img_news;
				  !empty(Session::get('page')) ? Session::forget('page') : false;
				  Session::put('page',URL::current());
			}

        $another_news = News::where('id', '!=', $new_id)->orderBy('created_at','desc')->take(3)->get();

		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
        return view('new',['id'=>$id,'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords,
		'content'=>$content, 'created_at'=>$created_at, 'updated_at'=>$updated_at, 'title_img_new'=>$title_img_new,
		'another_news'=>$another_news,'footer_contact' => $footer_contact]);
    }

	public function edit_news(Request $request) {

		$news = News::find($request->id);

		$news->update(['name' => $request->title, 'meta_keywords' => $request->meta_keywords, 'description' => $request->description, 'content' => $request->content_news, 'updated_at' => date('Y-m-d H:i:s')]);

		if( isset($request->title_img) ) {
			$path = 'public/uploads/news';
			file_exists($news->title_img_news) ? unlink($news->title_img_news) : false;
			if(!is_dir($path))
				mkdir($path,0777,true);
			$uniqid = uniqid();
			$request->title_img->move($path, 'news'.$news->id.$uniqid.'.'.$request->title_img->getClientOriginalExtension());
			$news->title_img_news = $path.'/news'.$news->id.$uniqid.'.'.$request->title_img->getClientOriginalExtension();
			$news->save();
			ResizeImage::ResizeImage($path.'/news'.$news->id.$uniqid.'.'.$request->title_img->getClientOriginalExtension(),$path.'/news'.$news->id.$uniqid.'.'.$request->title_img->getClientOriginalExtension());

		}
		$dbuser = "";
		$dbname = "";
		$command = "mysqldump -u ".$dbuser." world >";
		system($command);
		return back();
	}

	public function delete_news($id) {
		$news = News::find($id);
		$title = explode("/",$news->title_img_news);
		$path = 'public/uploads/news';
		file_exists($path.'/'.$title[count($title) - 1]) ? unlink($path.'/'.$title[count($title) - 1]) : false;
		$news->delete();
		return redirect("/news");
	}

	public function sort(Request $request) {
		$news_current = News::find($request->id);
		$priority_new_current  = $news_current->priority;
		$news = News::orderBy("priority","asc")->where("priority",">",$priority_new_current)->first();
		$news_current->update(["priority" => $news->priority]);
		$news->update(["priority" => $priority_new_current]);
		echo json_encode(1);
	}

	public function edit_title_img_news(Request $request) {
        if(isset($request->visualise)) {
            $title_img = Visuals::find($request->id);
            $title = explode("/", $title_img->title_img)[3];
            $type = $request->visualise->getClientOriginalExtension();
            $path = 'public/uploads/visuals';
            if (!is_dir($path))
                mkdir($path, 0777, true);
            if (file_exists($path . "/" . $title)) {
                $type_old = explode(".", $title)[1];
                $old = explode("_", explode(".", $title)[0])[0];
                rename($path . "/" . $title, $path . "/" . $old . "_old_" . date("H-i-s_Y-m-d") . "." . $type_old);
            }
            $new = explode("_", explode(".", $title)[0])[0];
            $src = $request->visualise;
            $filename = $path . "/" . $new . "_" . date("H-i-s_Y-m-d") . "." . $type;
            ImageHelper::imagecreatefromanytype($src, $filename, $request->w, $request->h, $request->div_w,
                $request->div_h, $request->x1, $request->y1, 1200, 900);
            $title_img->title_img = $filename;
            $title_img->save();
        } else if(isset($request->main)) {
            $title_img = Main_Slider::find($request->id);
            $title = explode("/", $title_img->title_img)[3];
            $type = $request->main->getClientOriginalExtension();
            $path = 'public/images/visuals';
            if (!is_dir($path))
                mkdir($path, 0777, true);
            if (file_exists($path . "/" . $title)) {
                $type_old = explode(".", $title)[1];
                $old = explode("_", explode(".", $title)[0])[0];
                rename($path . "/" . $title, $path . "/" . $old . "_old_" . date("H-i-s_Y-m-d") . "." . $type_old);
            }
            $new = explode("_", explode(".", $title)[0])[0];
            $src = $request->main;
            $filename = $path . "/" . $new . "_" . date("H-i-s_Y-m-d") . "." . $type;
            ImageHelper::imagecreatefromanytype($src, $filename, $request->w, $request->h, $request->div_w,
                $request->div_h, $request->x1, $request->y1, 1582, 452);

            $title_img->title_img = $filename;
            $title_img->save();
        }
		return back();
	}

	public function rss() {
		$news = News::orderBy('created_at','desc')->take(30)->get();
		$rss_news = RSSHelper::print_rss($news);
		return $rss_news;
	}
}