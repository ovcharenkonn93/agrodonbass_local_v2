<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use PDO;

class LoadPriceCSV extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	public function LoadPriceCultivation(Request $request)
	{
		
		$path = '/public/uploads/prices';
		//$filename = $request->file_csv->path();
        if( isset($request->file_csv) ) {
			if(!is_dir($path))
				mkdir($path,0777,true);

			//$request->file_csv->move($path.'/');
		}

        $csv = /*$path.'/'.*/$_FILES['file_csv']['tmp_name'];


        $query = sprintf("LOAD DATA local INFILE '%s'
			INTO TABLE `services_cultivation`
			FIELDS TERMINATED BY ';'
			ENCLOSED BY '\"'
			ESCAPED BY '\"'
			LINES TERMINATED BY '\r\n' (`id`,`service_type`, `instruments`, `price`)", addslashes($csv));

        \DB::connection()->getpdo()->exec("TRUNCATE TABLE services_cultivation");

        return \DB::connection()->getpdo()->exec($query);

	}
	 
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
