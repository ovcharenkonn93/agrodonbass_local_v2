<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Site_Pages\Site_Pages;
use App\Models\Users\Users;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use App\Models\Offices;
use App\Models\Contacts;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function index(){
        $page = Site_Pages::where('url','=','/login')->first();
        $footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
        return view("auth_admin",['title'=>$page->title,'description'=>$page->meta_description,'meta_keywords'=>$page->meta_keywords,'footer_contact'=>$footer_contact]);
    }

    public function change_password() {
//        $page = Site_Pages::where('url','=',$_SERVER['REQUEST_URI'])->first();
        $page = Site_Pages::where('url','=','/change_password')->first();
        $footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
        return view("change_password",['title'=>$page->title,'description'=>$page->meta_description,'meta_keywords'=>$page->meta_keywords,'footer_contact'=>$footer_contact]);
    }

    public function update_password(Request $request) {
        if(md5('sdfsdf34' . md5($request->old_password) . 'dsfsdfsd13') ==  Users::all()->first()->password) {
            $new_password = md5('sdfsdf34' . md5($request->new_password) . 'dsfsdfsd13');
            $user = Users::all()->first();
            $user->password = $new_password;
            $user->save();
            echo json_encode("login");
        } else return 0;
    }

    public function auth(Request $request) {
        if(isset($request->login) && isset($request->password)) {
            if(Users::all()->where('login',$request->login)
                           ->where('password',md5('sdfsdf34' . md5($request->password) . 'dsfsdfsd13'))->first()) {
                $user = Users::all()->where('login',$request->login)
                    ->where('password',md5('sdfsdf34' . md5($request->password) . 'dsfsdfsd13'))->first();
                Session::put('user',$user->id);
                $url = !empty(Session::get('page')) ? Session::get('page') : '/';
                return $url;
            }
            else return 0;
        }
        else {
            return 'error';
        }
    }

    public function logout() {
//        dd(Session::get('page'));
        $page = Session::get('page');
        if(!empty($page) && $page == '/add_news' ) {
            Session::flush();
            Auth::logout();
            return redirect("/news");
        }

        Session::flush();
        Auth::logout();
        return back();
    }
}
