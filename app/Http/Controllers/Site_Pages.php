<?php

namespace App\Http\Controllers;

use App\Models\Agricultural_Products;
use App\Models\Contacts;
use App\Models\For_Plants;
use App\Models\Models_News\News;
use App\Models\Offices;
use App\Models\Schedule;
use App\Models\Tariff_Trucking;
use App\Models\Trucking;
use App\Models\Vacancies;
use App\Models\Cultivation;
use App\Models\Documents;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ResizeImage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use PhpParser\Comment\Doc;

class Site_Pages extends Controller
{
	public function index($page_name) //Страницы без разделов
		{
			$page = \App\Models\Site_Pages\Site_Pages::where('url', '/'.$page_name)->get();
						
						if ($page->count()<1){ //ошибка 404
						return \View::make('errors/404');
					}		
			
			$contact = NULL;
			$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
			foreach ($page as $p) {
				$id = $p->id;
				$url=$p->url;
				$title=$p->title;
				$description=$p->meta_description;
				$meta_keywords=$p->meta_keywords;
				$picture = $p->picture;
				$documents = $p->documents;

//				 if($p->id_contact) {
//					 $contact[] = Contacts::find($p->id_contact);
//					 foreach(Contacts::orderBy("id","asc")->whereNotIn("id", [$contact[0]->id])->get() as $item) {
//						 $contact[] = $item;
//					 }
//				 }
				 
					
				Session::put('page',$p->url);

				if(!empty(Session::get('user'))) {
					$content = "<form class='ad-edit-form' method='post' action='" . URL::to("edit_page") . "'>
						<input type='hidden' name='_token' value='" . csrf_token() . "'>
						<input type='hidden' name='page' value='" . $p->url . "'>
						<div class='row'>
							<textarea name='_content' class='form-control edit_page' rows='5'>" . $p->content . "</textarea>
						</div>
						<div class='container-for-save-edited-text'>
							<button type='submit' class='btn btn-primary save-txt'>
								<i class='fa fa-floppy-o'></i>&nbsp;Сохранить изменения текста
							</button>
						</div></form>";
				} else {
					$content = $p->content;
				}

			}
			if (!$page || ($page_name == "add_news") && empty(Session::get('user'))) {
				return redirect("news");
			}

			if($this->is_document_page($page_name)) {

				$offices = Offices::whereHas('documents',function($q){
					$q->whereHas('page',function($q){
						preg_match ("/\/[^\/]*$/", $_SERVER['REQUEST_URI'], $matches);
						$request_uri = $matches[0];
						$q->where('url','=',$request_uri);
					});
				})->get();

				$all_offices = Offices::orderBy('name')->get();
				return \View::make('documents',["footer_contact"=>$footer_contact, 'title'=>$title, 'url'=>$url, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'content' => $content,'offices' => $offices,'all_offices'=>$all_offices,'contact' => $contact,"picture" => $picture]);
			}

			return \View::make($page_name,['id_page' => $id,'title'=>$title, 'url'=>$url, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'content' => $content, 'contact' => $contact, "footer_contact" => $footer_contact, "picture" => $picture,"documents"=>$documents]);
		}

	private function is_document_page($page_name){
		return $page_name == "registration_documents"||$page_name == "licenses"||$page_name == "typical_forms";
	}

	public function add_document(Request $request){

		preg_match ("/\/[^\/]*$/", $request->url, $matches);
		$request_uri = $matches[0];
		$page = \App\Models\Site_Pages\Site_Pages::where('url','=',$request_uri)->first();
		$dir = 'public/uploads/documents/';

		if($_FILES["file"]["size"]>config('upload_files.max_document_size')){
			return json_encode(false);
		}

		$pos = strrpos($_FILES['file']['name'],".");
		$fn = substr($_FILES['file']['name'],0,$pos);

		$url = $dir.$_FILES['file']['name'];

		if(file_exists($dir.$_FILES['file']['name'])){
			$url = $dir.(''.time()).$_FILES['file']['name'];
			copy($_FILES['file']['tmp_name'], $url);
		}
		else{
			copy($_FILES['file']['tmp_name'], $dir.$_FILES['file']['name']);
		}

		$document = new Documents(array(
			'name'=>$fn,
			'url'=>$url,
			'id_page'=>$page->id
		));

		$document->save();

		$offs=explode(",",$request->office[0]);

		foreach($offs as $office){
			$document->offices()->attach($office);
		}

		return json_encode($document->id);
	}

	public function update_document(Request $request){

		//$page = \DB::table('site_pages')->where('url','=',$request->url)->first();
		$ids = json_decode($request->ids);
		$names = json_decode($request->names);
		$offices = json_decode($request->offices);

		$dir = 'public/uploads/documents/';

		for($i=0;$i<count($ids);$i++){
			$file_id = $ids[$i];
			$document = Documents::find($file_id);

			if(count($document->offices)>1){
				$document->offices()->detach($offices[$i]);

				$pos = strrpos($document->url,".");
				$fe = substr($document->url,$pos,strlen($document->url)-1);
				if(file_exists($dir.$names[$i])){
					copy($document->url, $dir.time().$names[$i].$fe);
				}
				else{
					copy($document->url,$dir.$names[$i].$fe);
				}
				$new_doc = new Documents(array(
					'name'=>$names[$i],
					'url'=>$dir.$names[$i].$fe,
					'id_page'=>$document->id_page
				));
				$new_doc->save();
				$new_doc->offices()->attach($offices[$i]);
			}
			else{
				$document->name = $request->newName;
				$document->save();
			}
		}

		return json_encode($ids);
	}

	public function delete_document(Request $request){
		$file_id = $request->id;
		$file = Documents::find($file_id);
		$file_links = \DB::table('offices_documents')->where('id_document','=',''.$file_id)->count();
		if($file_links>1){
			$file->offices()->detach($request->office);
			return 0;
		}
		$res = @unlink($file->url);
		$file->delete();
		return "".$res;
	}

	public function edit_structure(Request $request){

		$ext = getimagesize($_FILES["file"]["tmp_name"]);
		if($ext[2]!=1 && $ext[2]!=2 && $ext[2]!=3){
			return json_encode(2);
		}
		if($_FILES["file"]["size"]>config('upload_files.max_image_size')){
			return json_encode(1);
		}

		@unlink("public/uploads/interface/about_us.png");
		copy($_FILES["file"]["tmp_name"], "public/uploads/interface/about_us.png");
		$correct = ResizeImage::ResizeImage("public/uploads/interface/about_us.png", "public/uploads/interface/about_us.png");
		if($correct==false){
			return json_encode(2);
		}
		return json_encode(0);
	}

	public function edit_page_picture(Request $request){

		preg_match ("/\/[^\/]*$/", $request->url, $matches);
		$request_uri = $matches[0];
		$page = \App\Models\Site_Pages\Site_Pages::where('url','=', $request_uri)->first();

		$ext = getimagesize($_FILES["file"]["tmp_name"]);
		if($ext[2]!=1 && $ext[2]!=2 && $ext[2]!=3){
			return json_encode(2);
		}
		if($_FILES["file"]["size"]>config('upload_files.max_image_size')){
			return json_encode(1);
		}
		$dir = "public/images/interface/";
		@unlink($page->picture);
		if(file_exists($dir.$_FILES["file"]["name"])){
			$pct = $dir.(''.time()).$_FILES["file"]["name"];
			copy($_FILES["file"]["tmp_name"], $pct);
			$page->picture = $pct;
		}
		else{
			copy($_FILES["file"]["tmp_name"], $dir.$_FILES["file"]["name"]);
			$page->picture = $dir.$_FILES["file"]["name"];
		}
		//На случай если нужно изменять размер
//		$correct = ResizeImage::ResizeImage($dir.$_FILES["file"]["name"], $dir.$_FILES["file"]["name"]);
//		if($correct==false){
//			return json_encode(2);
//		}


		$page->save();

		return json_encode(0);
	}

	public function edit_page(Request $request) {

		\App\Models\Site_Pages\Site_Pages::orderBy('id', 'asc')->where('url', $request->page)->first()->update(['content' => $request->_content]);
		return back();
	}

	public function edit_meta(Request $request){

		//preg_match ("/\/[^\/]*$/", $request->url, $matches);

		$request_uri = $request->url;
		$number = last(explode("/",$request_uri));
		if(is_numeric($number)){
			$model = News::find($number);
		}
		else
		{
			$model = \App\Models\Site_Pages\Site_Pages::where('url','=', $request_uri)->first();
		}

		$model->meta_keywords = $request->key;
		$model->meta_description = $request->desc;
		$model->save();
		return json_encode(true);
	}
		
	public function contacts() //Контакты
		{		
			$page = \DB::table('site_pages')->where('url', '/contacts')->get();
			$schedule = [];
			$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->meta_description;
				  $meta_keywords=$p->meta_keywords;

                  Session::forget('page');
                  Session::put('page',$p->url);

			}	
			
			$offices = \DB::table('offices')->get();
			$contacts = \DB::table('contacts')->get();
						
			foreach($offices as $office) {
				$offices1[] = (object) ['id' => $office->id, 'name' => $office->name, 
				'legal_addr' => $office->legal_addr, 'actual_addr' => $office->actual_addr, 'email' => $office->email];
			}
			foreach($contacts as $contact) {
				$contacts1[] = (object) ['id' => $contact->id, 'office' => $contact->office, 'contact_type' => $contact->contact_type, 
				'position' => $contact->position, 'name' => $contact->name, 'phone_number' => $contact->phone_number, 'email' => $contact->email];
			}

			foreach(Schedule::orderBy("id","asc")->get() as $item){
				$schedule[] = (object) ['id' => $item->id, 'weekday' => $item->weekday,
							   'start' => date("H:i",strtotime($item->start)),
							   'end' =>  date("H:i",strtotime($item->end)), 'day_off' =>$item->day_off];
			}

			return \View::make('contacts',['footer_contact'=>$footer_contact,'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'offices' => $offices1, 'contacts' => $contacts1, 'schedule' => $schedule]);
		}
		
	public function edit_contacts(Request $request) {
		$form = $request->form;

		if(isset($form["office"])) {
			foreach ($form["office"] as $item) {
				Offices::find($item['id'])->update(['name' => $item['offices_name'], 'legal_addr' => $item['legal_addr'], 'actual_addr' => $item['actual_addr'],
													'email' => $item['email']]);
			}
		}

		if(isset($form["contact"])) {
			foreach ($form["contact"] as $item) {
				Contacts::find($item['id'])->update(['name' => $item['contact_name'], 'phone_number' => $item['phone_number'], 'position' => $item['position'],
					'contact_type' => $item['contact_type'], 'email' => $item['email']]);
			}
		}

		if(isset($form["schedule"])) {
			foreach ($form["schedule"] as $item) {
				Schedule::find($item['id'])->update(['start' => $item['start'], 'end' => $item['end']]);
			}
		}

		if(isset($form["new_contact"])) {
			foreach($form["new_contact"] as $item) {
				Contacts::create([
					'name' => $item['contact_name'],
					'phone_number' => $item['phone_number'],
					'position' => $item['position'],
					'contact_type' => $item['contact_type'],
					'email' => $item['email'],
					'office' => $item['office_id']
				]);
			}
		}

		if(isset($form["delete_contact"])) {
			foreach ($form["delete_contact"] as $item) {
				Contacts::find($item)->delete();
			}
		}

		echo json_encode(1);
	}

	public function add_contacts(Request $request) {
		$form = $request->form;

		$office = Offices::create(['name' => $form["new_office"]["offices_name"], 'legal_addr' => $form["new_office"]["legal_addr"],
								   'actual_addr' => $form["new_office"]["actual_addr"]]);

		foreach($form["new_contact"] as $item) {
			$contact = new Contacts();
			$contact->contact_type = $item["contact_type"];
			$contact->name = $item["contact_name"];
			$contact->position = $item["position"];
			$contact->phone_number = $item["phone_number"];
			$contact->email = $item["email"];
			$office->contacts()->save($contact);
		}

		echo json_encode(1);
	}

	public function update_office_contact(Request $request) {
		$form = $request->form;
		$office = Offices::find($form['id']);
		if(isset($form)) {
			$office->update([
				'name' => $form['office_name'],
				'legal_addr' => $form['legal_addr'],
				'actual_addr' => $form['actual_addr'],
				'email' => $form['email']
			]);
		}
		if(isset($form["contacts"])) {
			foreach ($form["contacts"] as $item) {
				Contacts::find($item['contact_id'])->update([
					'name' => $item['contact_name'],
					'phone_number' => $item['contact_phone_number'],
					'position' => $item['contact_position'],
					'contact_type' => $item['contact_type'],
					'email' => $item['contact_email']]);
			}
		}
		if(isset($form["new_contacts"]) && count($form["new_contacts"]) > 0) {
			foreach($form["new_contacts"] as $item) {

				$contact = new Contacts();
				$contact->contact_type = $item["contact_type"];
				$contact->name = $item["contact_name"];
				$contact->position = $item["contact_position"];
				$contact->phone_number = $item["contact_phone_number"];
				$contact->email = $item["contact_email"];
				$office->contacts()->save($contact);

//				Contacts::create([
//					'name' => $item['contact_name'],
//					'phone_number' => $item['phone_number'],
//					'position' => $item['position'],
//					'contact_type' => $item['contact_type'],
//					'email' => $item['email'],
//					'office' => $item['office_id']
//				]);
			}
		}
		if(isset($form["delete_contacts"])) {
			foreach ($form["delete_contacts"] as $item) {
				Contacts::find($item)->delete();
			}
		}
		return json_encode(1);
	}

	public function delete_office($id) {
		$office = Offices::find($id);
		$office->contacts()->delete();
		$office->delete();
		return redirect("/contacts");
	}

	public function vacancies() {
		$page = \DB::table('site_pages')->where('url', '/vacancies')->get();
		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
		foreach ($page as $p) {
			$id = $p->id;
			$title = $p->title;
			$description = $p->meta_description;
			$meta_keywords = $p->meta_keywords;
			$url = $p->url;
			$picture = $p->picture;
			Session::put('page', $p->url);

			if($p->id_contact) {
				$contact[] = Contacts::find($p->id_contact);
				foreach(Contacts::orderBy("id","asc")->whereNotIn("id", [$contact[0]->id])->get() as $item) {
					$contact[] = $item;
				}
			}

			if (!empty(Session::get('user'))) {
				$content = "<form class='ad-edit-form' method='post' action='" . URL::to("edit_page") . "'>
						<input type='hidden' name='_token' value='" . csrf_token() . "'>
						<input type='hidden' name='page' value='" . $p->url . "'>
						<div class='row'>
							<textarea name='_content' class='form-control edit_page' rows='5'>" . $p->content . "</textarea>
						</div>
						<div class='container-for-save-edited-text'>
							<button type='submit' class='btn btn-primary save-txt'>
								<i class='fa fa-floppy-o'></i>&nbsp;Сохранить изменения текста
							</button>
						</div></form>";
			} else {
				$content = $p->content;
			}
		}

		$offices = Offices::orderBy("id", "asc")->get();
		$list = [];

		foreach($offices as $office) {
			if(count($office->vacancies) > 0) {
				$list[$office->id] = (object) ["id" => $office->id, "name" => $office->name, "vacancies" => null];
				foreach ($office->vacancies as $item) {
					$list[$office->id]->vacancies[] = (object) ["id" => $item->id, "vacancies" => $item->vacancies, "gender" => $item->gender == 1 ? "Мужской" : ($item->gender == 0 ? "Женский" : "Любой"),
																		  "education" => $item->education, "experience" => $item->experience, "employment" => $item->employment];
				}
			}
		}
		$departments = Offices::orderBy("id", "asc")->get();
		return view("vacancies", ['footer_contact'=>$footer_contact,'id_page' => $id, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, "url" => $url, "content" => $content, "offices" => $list,
								  'departments' => $departments, 'contact' => $contact, 'picture'=>$picture]);
	}

	public function add_vacancies(Request $request) {
//		dd($request->all());
		Vacancies::create(["office" => $request->department, "vacancies" => $request->vacancies, "gender" => $request->gender, "education" => $request->education,
						   "experience" => $request->experience, "employment" => $request->employment]);

		return back();
	}

	public function delete_vacancies($id) {
		$vacancies = Vacancies::find($id);
		$vacancies->delete();
		return back();
	}

	public function edit_vacancies(Request $request) {
		$vacancies = Vacancies::find($request->id);
		$gender = [];
		for($i = -1; $i <= 1; $i++) {
			if($vacancies->gender != $i) {
				$gender[] = (object) ["id" => $i, "gender" => $i == 1 ? "Мужской" : ($i == 0 ? "Женский" : "Любой")];
			}
		}
		$data = ["id" => $vacancies->id, "current_office" => Offices::find($vacancies->office),
				 "offices" => Offices::orderBy("id", "asc")->whereNotIn("id", [$vacancies->office])->get(),
				 "gender" => (object) [ "id" => $vacancies->gender, "name" => $vacancies->gender == 1 ? "Мужской" : ($vacancies->gender == 0 ? "Женский" : "Любой")],
				 "other_gender" => $gender, "education" => $vacancies->education, "employment" => $vacancies->employment, "experience" => $vacancies->experience,
				 "vacancies" => $vacancies->vacancies];
		echo json_encode($data);
	}

	public function update_vacancies(Request $request) {
//		dd($request->all());
		Vacancies::find($request->id)->update([
			"office" => $request->department,
			"vacancies" => $request->vacancies,
			"gender" => $request->gender,
			"education" => $request->education,
			"experience" => $request->experience,
			"employment" => $request->employment
		]);
		return back();
	}

	public function contact_get_data_by_id(Request $request) {
		echo json_encode(Contacts::find($request->id));
	}

	public function contact_edit_page(Request $request) {
		\App\Models\Site_Pages\Site_Pages::find($request->page)->update(["id_contact" => $request->name]);
		return back();
	}

	public function cultivation() {
		$page = \App\Models\Site_Pages\Site_Pages::where('url', '/cultivation')->get();
		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
		foreach ($page as $p) {
			$id = $p->id;
			$title = $p->title;
			$description = $p->meta_description;
			$meta_keywords = $p->meta_keywords;
			$url = $p->url;
			$picture = $p->picture;
			$documents = $p->documents;
			Session::put('page', $p->url);

			if($p->id_contact) {
				$contact[] = Contacts::find($p->id_contact);
				foreach(Contacts::orderBy("id","asc")->whereNotIn("id", [$contact[0]->id])->get() as $item) {
					$contact[] = $item;
				}
			}

			if (!empty(Session::get('user'))) {
				$content = "<form class='ad-edit-form' method='post' action='" . URL::to("edit_page") . "'>
						<input type='hidden' name='_token' value='" . csrf_token() . "'>
						<input type='hidden' name='page' value='" . $p->url . "'>
						<div class='row'>
							<textarea name='_content' class='form-control edit_page' rows='5'>" . $p->content . "</textarea>
						</div>
						<div class='container-for-save-edited-text'>
							<button type='submit' class='btn btn-primary save-txt'>
								<i class='fa fa-floppy-o'></i>&nbsp;Сохранить изменения текста
							</button>
						</div></form>";
			} else {
				$content = $p->content;
			}
		}

		$cultivation  = Cultivation::orderBy("id","asc")->get();
		$list = [];

		foreach ($cultivation as $item) {
			$list[] = (object) ["id" => $item->id, "plowing" => $item->plowing, "aggregates" => $item->aggregates,
								"cost_services" => $item->cost_services_max == NULL ? $item->cost_services : $item->cost_services."-".$item->cost_services_max,
							    "units" => $item->units];
		}

		$offices = Offices::whereHas('documents')->orderBy('name')->orderBy('name')->get();

		return view("cultivation", ['footer_contact'=>$footer_contact,'id_page' => $id, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, "url" => $url, "content" => $content,
			                      'contact' => $contact, "cultivation" => $list, 'picture'=>$picture, 'documents'=>$documents, 'offices'=>$offices]);
	}

	public function add_cultivation(Request $request) {
		Cultivation::create(["plowing" => $request->plowing, "aggregates" => $request->aggregates, "cost_services" => $request->cost_services,
							 "cost_services_max" => strlen($request->cost_services_max) ? str_replace(" ","",$request->cost_services_max) : NULL,
							 "units" => $request->units]);
		return back();
	}

	public function delete_cultivation($id) {
		$cultivation = Cultivation::find($id);
		$cultivation->delete();
		return back();
	}

	public function edit_cultivation(Request $request) {
		$cultivation = Cultivation::find($request->id);
		echo json_encode($cultivation);
	}

	public function update_cultivation(Request $request) {
//		dd($request->all());
		Cultivation::find($request->id)->update([
			"plowing" => $request->plowing,
			"aggregates" => $request->aggregates,
			"cost_services" => $request->cost_services,
			"cost_services_max" => strlen($request->cost_services_max) ? str_replace(" ","",$request->cost_services_max) : NULL,
			"units" => $request->units,
		]);
		return back();
	}

	//Добавление связанной кнопки

	public function add_btn(Request $request){
		$page = \App\Models\Site_Pages\Site_Pages::find($request->page);
		$page->documents()->attach($request->document,['name'=>$request->name]);
		return json_encode(true);
	}

	//Изменение связанных кнопок

	public function edit_btn(Request $request){
		$page = \App\Models\Site_Pages\Site_Pages::find($request->page);
		$ids = json_decode($request->ids);
		$names = json_decode($request->names);
		$documents = json_decode($request->documents);
		//return json_encode($ids);

//		$doc = $page->documents()->wherePivot('id','=',$ids[3])->first();
//		return json_encode(count($ids));

		for($i=0;$i<count($ids);$i=$i+1){
			$page->documents()->wherePivot('id','=',$ids[$i])->detach();
			$page->documents()->attach($documents[$i],['name'=>$names[$i]]);
		}

		return json_encode($request->ids);
	}

	//Удаление связанной кнопки
	public function delete_btn(Request $request){
		$page = \App\Models\Site_Pages\Site_Pages::find($request->page);
		$page->documents()->wherePivot('id','=',$request->id)->detach();
		return json_encode(true);
	}

	public function trucking() {
		$page = \App\Models\Site_Pages\Site_Pages::where('url', '/trucking')->get();
		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
		foreach ($page as $p) {
			$id = $p->id;
			$title = $p->title;
			$description = $p->meta_description;
			$meta_keywords = $p->meta_keywords;
			$url = $p->url;
			$picture = $p->picture;
			$documents = $p->documents;
			Session::put('page', $p->url);

			if($p->id_contact) {
				$contact[] = Contacts::find($p->id_contact);
				foreach(Contacts::orderBy("id","asc")->whereNotIn("id", [$contact[0]->id])->get() as $item) {
					$contact[] = $item;
				}
			}

			if (!empty(Session::get('user'))) {
				$content = "<form class='ad-edit-form' method='post' action='" . URL::to("edit_page") . "'>
						<input type='hidden' name='_token' value='" . csrf_token() . "'>
						<input type='hidden' name='page' value='" . $p->url . "'>
						<div class='row'>
							<textarea name='_content' class='form-control edit_page' rows='5'>" . $p->content . "</textarea>
						</div>
						<div class='container-for-save-edited-text'>
							<button type='submit' class='btn btn-primary save-txt'>
								<i class='fa fa-floppy-o'></i>&nbsp;Сохранить изменения текста
							</button>
						</div></form>";
			} else {
				$content = $p->content;
			}
		}

		$trucking = Trucking::orderBy("id", "asc")->get();
		$list = [];

		foreach($trucking as $key => $item) {
			$list[$key] = (object)["id" => $item->id, "type_work" => $item->type_work, "vehicle" => $item->vehicle, "tariff" => NULL];
//			$list[$key]->tariff .= "<ol>";
			foreach($item->tariff as $row) {
				$list[$key]->tariff[] = (object)["id" => $row->id, "id_trucking" => $row->id_trucking, "tariff" => $row->tariff];
//				$list[$key]->tariff .= "<li>".$row->tariff."</li>";
			}
//			$list[$key]->tariff .= "</ol>";
		}

		$offices = Offices::whereHas('documents')->orderBy('name')->orderBy('name')->get();

		return view("trucking", ['footer_contact'=>$footer_contact,'id_page' => $id, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, "url" => $url, "content" => $content,
			'contact' => $contact, "trucking" => $list,"picture"=>$picture,"documents"=>$documents,"offices"=>$offices]);
	}

	public function add_trucking(Request $request) {
//		dd($request->all());
		$trucking = Trucking::create(["type_work" => $request->type_work, "vehicle" => $request->vehicle]);
		foreach($request->tariff as $item) {
			$tariff = new Tariff_Trucking();
			$tariff->id_trucking = $trucking->id;
			$tariff->tariff = $item;
			$trucking->tariff()->save($tariff);
		}
		return back();
	}

	public function delete_trucking($id) {
		$trucking = Trucking::find($id);
		$trucking->tariff()->delete();
		$trucking->delete();
		return back();
	}

	public function edit_trucking(Request $request) {
		$trucking = Trucking::find($request->id);
		$data = (object)["id" => $trucking->id, "type_work" => $trucking->type_work, "vehicle" => $trucking->vehicle, "tariff" => NULL];
		foreach($trucking->tariff as $row) {
			$data->tariff[] = (object) (object)["id" => $row->id, "tariff" => $row->tariff];
		}
		echo json_encode($data);
	}

	public function update_trucking(Request $request) {
		$trucking = Trucking::find($request->id);
		$trucking->tariff()->delete();
		$trucking->update([
			"type_work" => $request->type_work,
			"vehicle" => $request->vehicle
		]);
		foreach($request->tariff as $item) {
			$tariff = new Tariff_Trucking();
			$tariff->id_trucking = $trucking->id;
			$tariff->tariff = $item;
			$trucking->tariff()->save($tariff);
		}
		return back();
	}

	public function for_plants() {
		$page = \App\Models\Site_Pages\Site_Pages::where('url', '/for_plants')->get();
		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
		foreach ($page as $p) {
			$id = $p->id;
			$title = $p->title;
			$description = $p->meta_description;
			$meta_keywords = $p->meta_keywords;
			$url = $p->url;
			$picture = $p->picture;
			$documents = $p->documents;
			Session::put('page', $p->url);

			if($p->id_contact) {
				$contact[] = Contacts::find($p->id_contact);
				foreach(Contacts::orderBy("id","asc")->whereNotIn("id", [$contact[0]->id])->get() as $item) {
					$contact[] = $item;
				}
			}

			if (!empty(Session::get('user'))) {
				$content = "<form class='ad-edit-form' method='post' action='" . URL::to("edit_page") . "'>
						<input type='hidden' name='_token' value='" . csrf_token() . "'>
						<input type='hidden' name='page' value='" . $p->url . "'>
						<div class='row'>
							<textarea name='_content' class='form-control edit_page' rows='5'>" . $p->content . "</textarea>
						</div>
						<div class='container-for-save-edited-text'>
							<button type='submit' class='btn btn-primary save-txt'>
								<i class='fa fa-floppy-o'></i>&nbsp;Сохранить изменения текста
							</button>
						</div></form>";
			} else {
				$content = $p->content;
			}
		}

		$for_plants = For_Plants::orderBy("id","asc")->get();
		$list = [];

		foreach ($for_plants as $item) {
			$list[] = (object)[
				"id" => $item->id,
				"name" => $item->name,
				"description" => $item->description,
				"cost" => $item->cost];
		}

		$offices = Offices::whereHas('documents')->orderBy('name')->orderBy('name')->get();

		return view("for_plants", ['footer_contact'=>$footer_contact,'id_page' => $id, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, "url" => $url, "content" => $content,
			'contact' => $contact, 'for_plants' => $list,'picture'=>$picture,'documents'=>$documents,'offices'=>$offices]);
	}

	public function add_for_plants(Request $request) {
		For_Plants::create([
			"name" => $request->name,
			"description" => $request->description,
			"cost" => $request->cost
		]);
		return back();
	}

	public function delete_for_plants($id) {
		For_Plants::find($id)->delete();
		return back();
	}

	public function edit_for_plants(Request $request) {
		$for_plants = For_Plants::find($request->id);
		$data = (object)["id" => $for_plants->id, "name" => $for_plants->name, "description" => $for_plants->description,
						 "cost" => $for_plants->cost];
		echo json_encode($data);
	}

	public function update_for_plants(Request $request) {
		For_Plants::find($request->id)->update([
			"name" => $request->name,
			"description" => $request->description,
			"cost" => $request->cost
		]);
		return back();
	}

	public function agricultural_products () {
		$page = \App\Models\Site_Pages\Site_Pages::where('url', '/agricultural-products')->get();
		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => [Contacts::find(1)->phone_number, Contacts::find(4)->phone_number]];
		foreach ($page as $p) {
			$id = $p->id;
			$title = $p->title;
			$description = $p->meta_description;
			$meta_keywords = $p->meta_keywords;
			$url = $p->url;
			$picture = $p->picture;
			$documents = $p->documents;
			Session::put('page', $p->url);

			if($p->id_contact) {
				$contact[] = Contacts::find($p->id_contact);
				foreach(Contacts::orderBy("id","asc")->whereNotIn("id", [$contact[0]->id])->get() as $item) {
					$contact[] = $item;
				}
			}

			if (!empty(Session::get('user'))) {
				$content = "<form class='ad-edit-form' method='post' action='" . URL::to("edit_page") . "'>
						<input type='hidden' name='_token' value='" . csrf_token() . "'>
						<input type='hidden' name='page' value='" . $p->url . "'>
						<div class='row'>
							<textarea name='_content' class='form-control edit_page' rows='5'>" . $p->content . "</textarea>
						</div>
						<div class='container-for-save-edited-text'>
							<button type='submit' class='btn btn-primary save-txt'>
								<i class='fa fa-floppy-o'></i>&nbsp;Сохранить изменения текста
							</button>
						</div></form>";
			} else {
				$content = $p->content;
			}
		}

		$agricultural_products = Agricultural_Products::orderBy("id","asc")->get();
		$list = [];

		foreach ($agricultural_products as $item) {
			$item->action == 0
			?	$list["sale"][] = (object)[
					"id" => $item->id,
					"name" => $item->name,
					"description" => $item->description,
					"cost" => $item->cost]
			:	$list["buy"][] = (object)[
				"id" => $item->id,
				"name" => $item->name,
				"description" => $item->description,
				"cost" => $item->cost];
		}

		$offices = Offices::whereHas('documents')->orderBy('name')->orderBy('name')->get();
		return view("agricultural-products", ['footer_contact'=>$footer_contact,'id_page' => $id, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, "url" => $url, "content" => $content,
			'contact' => $contact, 'agricultural_products' => $list,'picture'=>$picture,'documents'=>$documents,'offices'=>$offices]);
	}

	public function add_agricultural_products(Request $request) {
		Agricultural_Products::create([
			"name" => $request->name,
			"description" => $request->description,
			"cost" => $request->cost,
			"action" => $request->action
		]);
		return back();
	}

	public function delete_agricultural_products($id) {
		Agricultural_Products::find($id)->delete();
		return back();
	}

	public function edit_agricultural_products(Request $request) {
		$agricultural_products = Agricultural_Products::find($request->id);
		$data = (object)["id" => $agricultural_products->id, "name" => $agricultural_products->name, "description" => $agricultural_products->description,
			"cost" => $agricultural_products->cost, "action" => [
				(object) ["status" => $agricultural_products->action, "name" => $agricultural_products->action == 0 ? "Продажа" : "Покупка"],
				(object) ["status" => (int)!$agricultural_products->action, "name" => (int)!$agricultural_products->action == 0 ? "Продажа" : "Покупка"]
			]];
		echo json_encode($data);
	}

	public function update_agricultural_products(Request $request) {
		Agricultural_Products::find($request->id)->update([
			"name" => $request->name,
			"description" => $request->description,
			"cost" => $request->cost,
			"action" => $request->action
		]);
		return back();
	}
}
