<?php

namespace App\Http\Controllers\Controller_Cultivation;

use App\Http\Controllers\Controller;
use App\Models\Models_News\News;
use App\Models\Site_Pages\Site_Pages;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Controller_Cultivation extends Controller
{
    public function cultivation() {
        //Заголовок и описание страницы
        $page = \DB::table('site_pages')->where('url', '/cultivation')->first();
        if (!$page) {
            return \View::make('errors.404');
        }

        $title=$page->title;
        $description=$page->description;
        $meta_keywords=$page->keywords;

        !empty(Session::get('page')) ? Session::forget('page') : false;
        Session::put('page',$page->url);


        if(!empty(Session::get('user'))) {
            $content = "<form method='post' action='" . URL::to("cultivation/edit_cultivation") . "'>
						 <input type='hidden' name='_token' value='" . csrf_token() . "'>
						<div class='row'>
							<textarea name='_content' class='form-control edit_cultivation' rows='5'>" . $page->content . "</textarea>
						</div>
						<div class='row col-md-offset-10 col-md-2  margin-top-10px margin-bottom-10px'>
							<button type='submit' class='btn btn-block btn-flat btn-primary ad-news-link'>
								<i class='fa fa-floppy-o'></i>Сохранить
							</button>
						</div></form>";
        } else {
            $content = $page->content;
        }

        return view('cultivation',['title'=>$title, 'description'=>$description, 'content' =>$content, 'meta_keywords'=>$meta_keywords]);

    }

    public function edit_cultivation(Request $request) {
//		dd($request->all());
        Site_Pages::orderBy('id', 'asc')->where('url', '/cultivation')->first()->update(['content' => $request->_content]);
        return back();
    }

}