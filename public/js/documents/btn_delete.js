$('#dd').on('click',function(){
    var fd = new FormData();
    fd.append("id", $(this).data('id'));
    fd.append("page", $(this).data('page'));
    fd.append("_token",$("input[name='_token']").val());
    $.ajax({
        url: '/delete_btn',
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: fd,
        success: function (data) {
            location.reload(true);
        },
        error: function(errors)
        {
            console.log(errors);
        }
    });
});

$('.ddbtn').on('click', function(){
    $('#dd').data('id',$(this).data('pivid'));
    $('#dd').data('page',$(this).data('page'));
    $('#del_title').html("Вы хотите удалить кнопку?");
    $('#delete_document').modal('show');
});
