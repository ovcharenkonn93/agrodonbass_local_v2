$(document).ready(function(){
    $('#uploading').hide();
    $('#office :first').prop('selected',true);
});

$('#files').on('change',function(){
    $('#uploading').show();
    var fd = new FormData();
    fd.append("file", $('#files')[0].files[0]);
    fd.append("office[]",$('#office').val());
    fd.append("url",window.location.pathname);
    fd.append("_token",$("input[name='_token']").val());

    $.ajax({
        url: 'add_document',
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: fd,
        success: function (data) {
            if(!data){
                setTimeout(function() {
                    $.bootstrapGrowl("<span style='font-size:1vmax;'>Файл слишком большой!</span>", { type: 'danger',delay: 5000, align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
                }, 0);
                alert("Файл слишком большой!");
            }
            setTimeout(function() {
                $.bootstrapGrowl("<span style='font-size:1vmax;'>Документ успешно добавлен!</span>", { type: 'success',delay: 5000, align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
            }, 0);
            location.reload(true);
        },
        error: function(errors)
        {
            console.log(errors);
        }
    });
});

$('#dd').on('click',function(){
    var fd = new FormData();
    fd.append("id", $(this).data('id'));
    fd.append("office", $(this).data('office'));
    fd.append("_token",$("input[name='_token']").val());
    $.ajax({
        url: 'delete_document',
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: fd,
        success: function (data) {
            location.reload(true);
        },
        error: function(errors)
        {
            console.log(errors);
        }
    });
});

$('.ddbtn').on('click', function(){
    $('#dd').data('id',$(this).data('id'));
    $('#dd').data('office',$(this).data('office'));
    $('#del_title').html("Вы хотите удалить документ?");
    $('#delete_document').modal('show');
});

$('#save').on('click', function(){
    var ids = Array();
    var names = Array();
    var offices = Array();
    $('.file-name').each(function(){
        if($(this).val().trim()!=$(this).data('original')){
            //alert();
            ids.push($(this).data('id'));
            names.push($(this).val().trim());
            offices.push($(this).data('office'));
        }
    });
    if(ids.length!=0){
        var fd = new FormData();
        fd.append("ids", JSON.stringify(ids));
        fd.append("offices", JSON.stringify(offices));
        fd.append("names",JSON.stringify(names));
        fd.append("_token",$("input[name='_token']").val());
        $.ajax({
            url: '/update_document',
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: fd,
            success: function (data) {
                location.reload(true);
            },
            error: function(errors)
            {
                console.log(errors);
            }
        });
    }
    //location.reload(true);
});