$(".btn_edit").on('click', function(){

    var is_edited = false;
    var ids=Array(),names=Array(),documents=Array();
    $(".btn_name").each(function(){
        if($(this).data('origin')!=$(this).val().trim()){
            is_edited = true;
            ids.push($(this).data('pivid'));
            names.push($(this).val().trim());
        }
    });
    $(".btn_document").each(function(){
        if($(this).data('origin')!=$(this).val()){
            is_edited = true;
            documents.push($(this).val());
        }
    });
    if(!is_edited){
        return false;
    }
    var fd = new FormData();
    fd.append("ids", JSON.stringify(ids));
    fd.append("names", JSON.stringify(names));
    fd.append("documents", JSON.stringify(documents));
    fd.append("page", $(this).data('page'));
    fd.append("_token",$("input[name='_token']").val());
    $.ajax({
        url: '/edit_btn',
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: fd,
        success: function (data) {
            location.reload(true);
        },
        error: function(errors)
        {
            console.log(errors);
        }
    });

});
