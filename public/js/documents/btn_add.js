$(".btn_add").on('click', function(){
    if($("#btn_name_add").val().trim()==""){
        alert("Имя не может быть пустым!");
    }
    else{
        var fd = new FormData();
        fd.append("name", $("#btn_name_add").val().trim());
        fd.append("document", $("#btn_document_add").val());
        fd.append("page",$(this).data('page'));
        fd.append("_token",$("input[name='_token']").val());
        $.ajax({
            url: '/add_btn',
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: fd,
            success: function (data) {
                location.reload(true);
            },
            error: function(errors)
            {
                console.log(errors);
            }
        });
    }
});