$(document).ready(function() {
    $("button#add_trucking").click(function() {
        $("div.add_trucking").modal("show");
    });

    $("i.add_tariff").click(function() {
        $(this).parent().parent().parent().append(
            "<div class='row margin-top-10px'>" +
                "<div class='col-md-offset-6 col-md-5'><input type='text' name='tariff[]' class='form-control' required></div>" +
                "<div class='col-md-1'><i class='fa fa-minus delete_tariff' style='cursor: pointer'></i></div>" +
            "</div>"
        );
    });

    $("div.add_trucking, div.edit_trucking").on("click", "i.delete_tariff", function() {
        $(this).parent().parent().remove();
    });

    $("a.delete_trucking").click(function() {
        $("div.delete_trucking").modal("show");
        $("div.delete_trucking a#delete_trucking").attr("href",$(this).attr("data-href"))
    });

    $("a.edit_trucking").click(function() {
        $.ajax({
            url: 'trucking/edit',
            type:'get',
            dataType: 'json',
            data: {
                id: $(this).attr("data-id")
            },
            success: function(response) {
                console.log(response);
                $("input[name='type_work']").val(response.type_work);
                $("input[name='vehicle']").val(response.vehicle);
                $("input[name='tariff[]']").val(response.tariff[0].tariff);
                var first_tariff = $("input[name='tariff[]']");
                for(var i = 1; i < response.tariff.length; i++) {
                    first_tariff.parent().parent().parent().append(
                        "<div class='row margin-top-10px'>" +
                        "<div class='col-md-offset-6 col-md-5'><input type='text' name='tariff[]' class='form-control' required></div>" +
                        "<div class='col-md-1'><i class='fa fa-minus delete_tariff' style='cursor: pointer'></i></div>" +
                        "</div>"
                    );
                    $("input[name='tariff[]']:last").val(response.tariff[i].tariff);
                }
                $("input[name='id']").val(response.id);
                $("div.edit_trucking").modal("show");
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});