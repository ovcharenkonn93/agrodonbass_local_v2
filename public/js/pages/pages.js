$(document).ready(function() {
    tinymce.init({
        language: 'ru',
        theme: 'modern',
        relative_urls: false,
        selector: 'textarea.edit_page',
        plugins : [
            'advlist autolink lists link preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern'],
        menubar: 'edit insert view format table tools',
        toolbar1: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor emoticons',
        image_advtab: true
    });

    $('.save-txt').on('click',function(){
        tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent().replace(/<p>&nbsp;<\/p>/g,""));
        tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent().replace(/<p><br \/>/g,"<p>"));
    });

    $("select[name='name']").change(function() {
        var id = $("select#name option:selected").val();
        $.ajax({
            url:"contact/get_data",
            type:"GET",
            dataType:"json",
            data: {
                id: id
            },
            success: function(response) {
                console.log(response);
                $("p#phone_number").text(response.phone_number + " ");
                $("p#email").text(response.email);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });



    //$("#imgInput").change(function(){
    //    readURL(this);
    //});

    $("#hpic").on('change',function(){
        im_type = "hpic";
        //alert();
        $('#old_image').attr('src',$('#title_image').attr('src'));
        readURL(this,'new_image');
        $('#edit_image_modal').modal('show');
    });

    //$("#edit_structure").on('change',function(){
    //    $('#edit_image_modal').modal('show');
    //});
});