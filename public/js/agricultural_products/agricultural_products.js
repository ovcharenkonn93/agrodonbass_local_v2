$(document).ready(function() {
    $("button#add_agricultural_products").click(function() {
        var action = $(this).attr("data-action");
        var text = action == 0 ? "Добавление расценки на продажу сельхозпродукции" : "Добавление расценки на покупку сельхозпродукции";
        $("div.add_agricultural_products h4.title").text(text);
        $("div.add_agricultural_products input[name='action']").val(action);
        $("div.add_agricultural_products").modal("show");
    });

    //$('input[name="cost"]').inputmask("numeric", {
    //    radixPoint: ".",
    //    groupSeparator: " ",
    //    autoGroup: true,
    //    rightAlign: false
    //});

    $("a.delete_agricultural_products").click(function() {
        $("div.delete_agricultural_products").modal("show");
        $("div.delete_agricultural_products a#delete_agricultural_products").attr("href",$(this).attr("data-href"))
    });

    $("a.edit_agricultural_products").click(function() {
        $("select[name='action']").empty();
        $.ajax({
            url: 'agricultural-products/edit',
            type:'get',
            dataType: 'json',
            data: {
                id: $(this).attr("data-id")
            },
            success: function(response) {
                console.log(response);
                $("input[name='name']").val(response.name);
                $("input[name='cost']").val(response.cost);
                $("textarea[name='description']").text(response.description);
                $("input[name='id']").val(response.id);

                for(var i = 0; i < response.action.length; i++) {
                    $("select[name='action']").append(
                        "<option value='"+response.action[i].status+"'>" + response.action[i].name + "</option>"
                    )
                }
                $("div.edit_agricultural_products").modal("show");
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});