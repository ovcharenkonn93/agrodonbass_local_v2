$(document).ready(function() {
    $("button#add_for_plants").click(function() {
        $("div.add_for_plants").modal("show");
    });

    //$('input[name="cost"]').inputmask("numeric", {
    //    radixPoint: ".",
    //    groupSeparator: " ",
    //    autoGroup: true,
    //    rightAlign: false
    //});

    $("a.delete_for_plants").click(function() {
        $("div.delete_for_plants").modal("show");
        $("div.delete_for_plants a#delete_for_plants").attr("href",$(this).attr("data-href"))
    });

    $("a.edit_for_plants").click(function() {
        $.ajax({
            url: 'for_plants/edit',
            type:'get',
            dataType: 'json',
            data: {
                id: $(this).attr("data-id")
            },
            success: function(response) {
                console.log(response);
                $("input[name='name']").val(response.name);
                $("input[name='cost']").val(response.cost);
                $("textarea[name='description']").text(response.description);
                $("input[name='id']").val(response.id);
                $("div.edit_for_plants").modal("show");
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});