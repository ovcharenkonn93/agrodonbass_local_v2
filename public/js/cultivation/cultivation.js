$(document).ready(function() {
    tinymce.init({
        language: 'ru',
        theme: 'modern',
        relative_urls: false,
        selector: 'textarea.edit_cultivation',
        plugins : [
            'advlist autolink lists link preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern'],
        toolbar1: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor emoticons',
        image_advtab: true
    });

    $('input[name="cost_services"], input[name="cost_services_max"]').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: " ",
        autoGroup: true,
        rightAlign: false
    });

    $("a.delete_cultivation").click(function() {
        $("div.delete_cultivation").modal("show");
        $("div.delete_cultivation a#delete_cultivation").attr("href",$(this).attr("data-href"))
    });

    $("button#add_cultivation").click(function() {
        $("div.add_cultivation").modal("show");
    });

    $("a.edit_cultivation").click(function() {
        $.ajax({
            url: 'cultivation/edit',
            type:'get',
            dataType: 'json',
            data: {
                id: $(this).attr("data-id")
            },
            success: function(response) {
                console.log(response);
                $("input[name='plowing']").val(response.plowing);
                $("input[name='aggregates']").val(response.aggregates);
                $("input[name='cost_services']").val(response.cost_services);
                $("input[name='cost_services_max']").val(response.cost_services_max);
                $("input[name='units']").val(response.units);
                $("input[name='id']").val(response.id);
                $("div.edit_cultivation").modal("show");
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});