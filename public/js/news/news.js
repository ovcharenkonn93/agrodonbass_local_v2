var x1, y1, x2, y2, crop = 'crop/';
var  jcrop_api;
$(document).ready(function() {
    tinymce.init({
        language: 'ru',
        theme: 'modern',
        relative_urls: false,
        selector: 'textarea.news',
        plugins : ['jbimages',
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'],
        menubar: 'edit insert view format table tools',
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages',
        toolbar2: 'print preview | forecolor backcolor emoticons',
        image_advtab: true
    });

    $("a.delete_news").click(function() {
        $("div.delete_news").modal("show");
        $("div.delete_news a#delete_news").attr("href",$(this).attr("data-url"))
    });

    $("div.ad-news").on("click","a.sort_news",function(event) {
        event.preventDefault();
        var $this = $(this).parents("div.ad-new1");
        var news_block = $(this).parents("div.ad-new1");
        var prev_news_block = $(this).parents("div.ad-new1").prev("div.ad-new1");
        var url = $(this).attr("data-href");
        var id = $(this).attr("data-id");
        var fl = prev_news_block.length;
        $.ajax({
            url: url,
            type: 'GET',
            dataType: "json",
            data: {id: id},
            success: function(response) {
                console.log(response);
                if(fl) {
                    $this.remove();
                    prev_news_block.before(news_block);
                    //parseInt(location.href.split("?")[1].split("=")[1]) == 1
                    //    ? ($("div.ad-new1:first a.sort_news").length ? $("div.ad-new1:first a.sort_news").remove() : false)
                    //    : false
                } else {
                    location.replace(location.href.split("?")[0] + "?page=" + (parseInt(location.href.split("?")[1].split("=")[1]) - 1));
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    $("button.title_img_modal").click(function() {

        $("div.edit_title_img_news").modal("show");
        $("div.edit_title_img_news input[type='file']").attr('name',$(this).attr('data-type'));
        typeof jcrop_api != "undefined" ? jcrop_api.destroy() : false;
        $("div.edit_title_img_news input[name='id']").val($(this).attr("data-id"));
        $("div.edit_title_img_news input[name='title_img']").val("");
        $("div.edit_title_img_news img#title_img").attr('src',$(this).attr("data-img"));
        $('img#title_img').attr('width','100%');
        $('img#title_img').attr('height','auto');
    });



    $("div.edit_title_img_news").on("change", "input[name='visualise']", function(event) {
        event.preventDefault();
        if(this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                typeof jcrop_api != "undefined" ? jcrop_api.destroy() : false;
                var x = 0, y = 0, x_2 = 0, y_2 = 0;
                $("div.edit_title_img_news div.modal-dialog").css({
                    width: "70%"
                });
                $('img#title_img').attr('src', e.target.result);
                $('img#title_img').attr('width','100%');
                $('img#title_img').attr('height','auto');
                setTimeout(function() {
                    $("input[name='div_w']").val($("img#title_img").width());
                    $("input[name='div_h']").val($("img#title_img").height());

                    if($("img#title_img").width() > $("img#title_img").height()) {
                        y = 0;
                        y_2 = $("img#title_img").height();
                        var width = ($("img#title_img").height()/3) * 4;
                        x = $("img#title_img").width()/2 - width/2;
                        x_2 = $("img#title_img").width()/2 + width/2;
                    } else {
                        x = 0;
                        x_2 = $("img#title_img").width();
                        var height = $("img#title_img").width()/4 * 3;
                        y = $("img#title_img").height()/2 - height/2;
                        y_2 = $("img#title_img").height()/2 + height/2;
                    }
                    jcrop_api = $.Jcrop($('img#title_img'),{
                        onChange: updateCoords,
                        onSelect: updateCoords,
                        minSize:  [ 4, 3 ],
                        maxSize:  [ 1200, 900 ],
                        aspectRatio: 4/3,
                        setSelect: [x, y, x_2, y_2]
                    });
                }, 300);

            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $(" div.edit_title_img_news").on("change", "input[name='main']", function(event) {
        event.preventDefault();
        if(this.files && this.files[0]) {
            var type = this.files[0].type.split('/')[1];
            if(type == "jpg" || type == "jpeg" || type == "png" || type == "gif") {
                var reader = new FileReader();
                reader.onload = function (e) {
                    typeof jcrop_api != "undefined" ? jcrop_api.destroy() : false;
                    var x = 0, y = 0, x_2 = 0, y_2 = 0;
                    $("div.edit_title_img_news div.modal-dialog").css({
                        width: "70%"
                    });
                    $('img#title_img').attr('src', e.target.result);
                    $('img#title_img').attr('width', '100%');
                    $('img#title_img').attr('height', 'auto');
                    setTimeout(function () {
                        $("input[name='div_w']").val($("img#title_img").width());
                        $("input[name='div_h']").val($("img#title_img").height());

                        if ($("img#title_img").width() < $("img#title_img").height()) {
                            y = 0;
                            y_2 = $("img#title_img").height();
                            var width = ($("img#title_img").height() / 2) * 7;
                            x = $("img#title_img").width() / 2 - width / 2;
                            x_2 = $("img#title_img").width() / 2 + width / 2;
                        } else {
                            x = 0;
                            x_2 = $("img#title_img").width();
                            var height = $("img#title_img").width() / 7 * 2;
                            y = $("img#title_img").height() / 2 - height / 2;
                            y_2 = $("img#title_img").height() / 2 + height / 2;
                        }
                        jcrop_api = $.Jcrop($('img#title_img'), {
                            onChange: updateCoords,
                            onSelect: updateCoords,
                            minSize: [7, 2],
                            maxSize: [1582, 452],
                            aspectRatio: 7 / 2,
                            setSelect: [x, y, x_2, y_2]
                        });
                    }, 300);

                };
                reader.readAsDataURL(this.files[0]);
            } else {
                $("div.wrong_type_img").modal("show");
            }
        }
    });
});

function updateCoords(c){
    x1 = c.x;
    y1 = c.y;
    x2 = c.x2;
    y2 = c.y2;

    $("input[name='x1']").val(x1);
    $("input[name='y1']").val(y1);
    $("input[name='x2']").val(x2);
    $("input[name='y2']").val(y2);
    $("input[name='w']").val(c.w);
    $("input[name='h']").val(c.h);
}
